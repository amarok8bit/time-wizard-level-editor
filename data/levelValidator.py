from data.enums import ActivationMethodKind, ProblemKind, EnemyKind
from data.levelInfo import LevelInfo, ItemInfo
from data.ranges import Ranges


class LevelProblem:

    def __init__(self, kind: ProblemKind, item: ItemInfo):
        self.kind = kind
        self.item = item

    def __str__(self):
        if self.item is None:
            return self.kind.value
        else:
            return f'{self.kind.value} - {self.item.getItemName()}'


class LevelValidator:

    def __init__(self, levelInfo: LevelInfo):
        self.levelInfo = levelInfo
        self.problems = []
        self.maxX = Ranges.xPos.max
        self.maxY = Ranges.yPos.max

    def validate(self):
        self.problems.clear()

        self.checkForProblemOutsideBoard(self.levelInfo.levelPortalIn)
        self.checkForProblemOutsideBoard(self.levelInfo.levelPortalOut)

        for item in self.levelInfo.boardItems:
            self.checkForProblemOutsideBoard(item)

        for item in self.levelInfo.movingPlatforms:
            self.checkForProblemOutsideBoard(item)

        for item in self.levelInfo.enemies:
            self.checkForProblemOutsideBoard(item)

        for item in self.levelInfo.lasers:
            self.checkForProblemOutsideBoard(item)

        for item in self.levelInfo.buttons:
            self.checkForProblemOutsideBoard(item)

        for item in self.levelInfo.forceFields:
            self.checkForProblemOutsideBoard(item)

        for item in self.levelInfo.disappearingPlatforms:
            self.checkForProblemOutsideBoard(item)

        for item in self.levelInfo.disappearingLadders:
            self.checkForProblemOutsideBoard(item)

        for item in self.levelInfo.hourglasses:
            self.checkForProblemOutsideBoard(item)

        for item in self.levelInfo.portals:
            self.checkForProblemOutsideBoard(item)

        if self.levelInfo.general.actionForAllHourglasses:
            for button in self.levelInfo.buttons:
                if button.state == self.levelInfo.general.stateForAllHourglasses:
                    self.problems.append(LevelProblem(ProblemKind.StateUsedForAllHourglasses, button))

        if len(self.levelInfo.hourglasses) == 0:
            self.problems.append(LevelProblem(ProblemKind.LackOfHourglasses, None))

        if len(self.levelInfo.movingPlatforms) > 8:
            self.problems.append(LevelProblem(ProblemKind.TooManyMovingPlatforms, self.levelInfo.movingPlatforms[-1]))

        if len(self.levelInfo.enemies) > 4:
            self.problems.append(LevelProblem(ProblemKind.TooManyEnemies, self.levelInfo.enemies[-1]))

        for enemy in self.levelInfo.enemies:
            if enemy.kind.isFollowing() and not enemy.timeResist:
                self.problems.append(LevelProblem(ProblemKind.FollowingEnemyNotTimeResist, enemy))

        if len(self.levelInfo.lasers) > 8:
            self.problems.append(LevelProblem(ProblemKind.TooManyLasers, self.levelInfo.lasers[-1]))

        if (len(self.levelInfo.forceFields) + len(self.levelInfo.disappearingPlatforms) +
                len(self.levelInfo.disappearingLadders) > 16):
            if len(self.levelInfo.forceFields) > 0:
                self.problems.append(LevelProblem(ProblemKind.TooManyActivableItems, self.levelInfo.forceFields[-1]))
            elif len(self.levelInfo.disappearingPlatforms) > 0:
                self.problems.append(LevelProblem(ProblemKind.TooManyActivableItems,
                                                  self.levelInfo.disappearingPlatforms[-1]))
            else:
                self.problems.append(LevelProblem(ProblemKind.TooManyActivableItems,
                                                  self.levelInfo.disappearingLadders[-1]))

        if len(self.levelInfo.buttons) > 8:
            self.problems.append(LevelProblem(ProblemKind.TooManyButtons, self.levelInfo.buttons[-1]))

        for button in self.levelInfo.buttons:
            if button.kind.isTimer() and not button.timeResist:
                self.problems.append(LevelProblem(ProblemKind.TimerButtonNotTimeResist, button))
        if len(self.levelInfo.states) > 8:
            self.problems.append(LevelProblem(ProblemKind.TooManyStates, self.levelInfo.states[-1]))

        if len(self.levelInfo.hourglasses) > 32:
            self.problems.append(LevelProblem(ProblemKind.TooManyHourglasses, self.levelInfo.hourglasses[-1]))

        if len(self.levelInfo.portals) > 16:
            self.problems.append(LevelProblem(ProblemKind.TooManyPortals, self.levelInfo.portals[-1]))

        if len(self.levelInfo.portals) % 2 == 1:
            self.problems.append(LevelProblem(ProblemKind.PortalWithoutPair, self.levelInfo.portals[-1]))

        for i in range(len(self.levelInfo.portals) // 2):
            index = 2 * i
            if self.levelInfo.portals[index].kind.isVertical() != self.levelInfo.portals[index + 1].kind.isVertical():
                self.problems.append(LevelProblem(ProblemKind.PortalPairOrientation, self.levelInfo.portals[index]))

        self.checkForProblemOverlap(self.levelInfo.levelPortalIn)
        self.checkForProblemOverlap(self.levelInfo.levelPortalOut)

        for item in self.levelInfo.movingPlatforms:
            self.checkForProblemOverlap(item)

        for item in self.levelInfo.enemies:
            self.checkForProblemOverlap(item)

        for item in self.levelInfo.lasers:
            self.checkForProblemOverlap(item)

        for item in self.levelInfo.buttons:
            self.checkForProblemOverlap(item)

        for item in self.levelInfo.forceFields:
            self.checkForProblemOverlap(item)

        for item in self.levelInfo.disappearingPlatforms:
            self.checkForProblemOverlap(item)

        for item in self.levelInfo.disappearingLadders:
            self.checkForProblemOverlap(item)

        for item in self.levelInfo.hourglasses:
            self.checkForProblemOverlap(item)

        for item in self.levelInfo.portals:
            self.checkForProblemOverlap(item)

        for item in self.levelInfo.buttons:
            self.checkForProblemState(item)

        for item in self.levelInfo.forceFields:
            if item.methodKind == ActivationMethodKind.State:
                self.checkForProblemState(item)

        for item in self.levelInfo.disappearingPlatforms:
            if item.methodKind == ActivationMethodKind.State:
                self.checkForProblemState(item)

        for item in self.levelInfo.disappearingLadders:
            if item.methodKind == ActivationMethodKind.State:
                self.checkForProblemState(item)

        for index, item in enumerate(self.levelInfo.states):
            self.checkForWrongTimeResist(index, item.timeResist)

    def checkForProblemOutsideBoard(self, item: ItemInfo):
        rect = item.getRect()
        if (rect.x() < 0) or (rect.right() > self.maxX) or (rect.y() < 0) or (rect.bottom() > self.maxY):
            self.problems.append(LevelProblem(ProblemKind.OutsideBoard, item))

    def checkForProblemOverlap(self, item: ItemInfo):
        rects = item.getRects()

        self.checkForProblemOverlapPair(item, rects, self.levelInfo.levelPortalIn)
        self.checkForProblemOverlapPair(item, rects, self.levelInfo.levelPortalOut)

        for another in self.levelInfo.movingPlatforms:
            self.checkForProblemOverlapPair(item, rects, another)

        for another in self.levelInfo.enemies:
            self.checkForProblemOverlapPair(item, rects, another)

        for another in self.levelInfo.lasers:
            self.checkForProblemOverlapPair(item, rects, another)

        for another in self.levelInfo.buttons:
            self.checkForProblemOverlapPair(item, rects, another)

        for another in self.levelInfo.forceFields:
            self.checkForProblemOverlapPair(item, rects, another)

        for another in self.levelInfo.disappearingPlatforms:
            self.checkForProblemOverlapPair(item, rects, another)

        for another in self.levelInfo.disappearingLadders:
            self.checkForProblemOverlapPair(item, rects, another)

        for another in self.levelInfo.hourglasses:
            self.checkForProblemOverlapPair(item, rects, another)

        for another in self.levelInfo.portals:
            self.checkForProblemOverlapPair(item, rects, another)

    def checkForProblemOverlapPair(self, item: ItemInfo, rects: list, another: ItemInfo):
        if item == another:
            return

        anotherRects = another.getRects()
        for rect in rects:
            for anotherRect in anotherRects:
                if rect.intersects(anotherRect):
                    self.problems.append(LevelProblem(ProblemKind.ObjectOverlap, item))
                    return

    def checkForProblemState(self, item: ItemInfo):
        if item.state >= len(self.levelInfo.states):
            self.problems.append(LevelProblem(ProblemKind.StateDoesNotExist, item))

    def checkForWrongTimeResist(self, index: int, timeResist: bool):
        for button in self.levelInfo.buttons:
            if (button.state == index) and (button.timeResist != timeResist):
                self.problems.append(LevelProblem(ProblemKind.InvalidTimeResist, button))

        for forceField in self.levelInfo.forceFields:
            if ((forceField.methodKind == ActivationMethodKind.State) and (forceField.state == index) and
                    (forceField.timeResist != timeResist)):
                self.problems.append(LevelProblem(ProblemKind.InvalidTimeResist, forceField))

        for platform in self.levelInfo.disappearingPlatforms:
            if ((platform.methodKind == ActivationMethodKind.State) and (platform.state == index) and
                    (platform.timeResist != timeResist)):
                self.problems.append(LevelProblem(ProblemKind.InvalidTimeResist, platform))

        for ladder in self.levelInfo.disappearingLadders:
            if ((ladder.methodKind == ActivationMethodKind.State) and (ladder.state == index) and
                    (ladder.timeResist != timeResist)):
                self.problems.append(LevelProblem(ProblemKind.InvalidTimeResist, ladder))
