import numpy as np
from PyQt6.QtCore import QFile, QIODevice

from data.binImportExportHelper import BinImportExportHelper
from data.enums import EnemyKind, ActivationMethodKind, ActivableItemKind
from data.levelInfo import LevelInfo, GeneralInfo, LevelPortalInInfo, LevelPortalOutInfo, PixelInfo


class BinLevelExporter:

    end = 255

    def __init__(self):
        self.helper = BinImportExportHelper()

    def export(self, levelInfo: LevelInfo):
        data = list()
        data.extend(self.encodeGeneral(levelInfo.general))
        data.extend(self.encodeBoardItems(levelInfo.boardItems))
        data.extend(self.encodeLevelPortals(levelInfo.levelPortalIn, levelInfo.levelPortalOut))
        data.extend(self.encodeMovingPlatforms(levelInfo.movingPlatforms))
        data.extend(self.encodeEnemies(levelInfo.enemies))
        data.extend(self.encodeLasers(levelInfo.lasers))
        data.extend(self.encodeStates(levelInfo.states))
        data.extend(self.encodeActivableItems(levelInfo))
        data.extend(self.encodeButtons(levelInfo.buttons))
        data.extend(self.encodeHourglasses(levelInfo.hourglasses))
        data.extend(self.encodePortals(levelInfo.portals))
        return data

    def save(self, levelInfo: LevelInfo, fileName: str):
        array = np.array(self.export(levelInfo), dtype=np.uint8)
        array.tofile(fileName)

    def exportTest(self, levelInfo: LevelInfo, fileName: str):
        data = self.export(levelInfo)
        file = QFile(':/game/game')
        file.open(QIODevice.OpenModeFlag.ReadOnly)
        game = list(file.readAll().data())
        offset = 11503
        game[offset:(offset+len(data))] = data
        result = np.array(game, dtype=np.uint8)
        result.tofile(fileName)

    def encodeGeneral(self, info: GeneralInfo):
        return [info.columns, self.helper.encodeBoardColor(info.color), info.seconds, info.actionForAllHourglasses,
                info.stateForAllHourglasses]

    def encodeBoardItems(self, items: list):
        data = list()
        for item in items:
            data.extend([self.helper.encodeBoardItemKind(item.kind), item.column, item.row])
            if item.kind.containsWidth():
                data.append(item.width)
            if item.kind.containsHeight():
                data.append(item.height)
        data.append(self.end)
        return data

    @staticmethod
    def encodeLevelPortals(portalIn: LevelPortalInInfo, portalOut: LevelPortalOutInfo):
        return [portalIn.column, portalIn.row, portalIn.height, portalOut.column, portalOut.row, portalOut.height]

    def encodeMovingPlatforms(self, items: list):
        data = list()
        for item in items:
            data.extend([item.xPos, item.row, item.width, self.helper.encodeRangeKind(item.range), item.moveOffset,
                         self.helper.encodeBool(item.timeResist)])
        data.append(self.end)
        return data

    def encodeEnemies(self, items: list):
        data = list()
        for item in items:
            if item.kind == EnemyKind.Horizontal or item.kind == EnemyKind.FollowingHorizontal:
                data.extend([item.xPos, PixelInfo.roundYPos(item.yPos)])
            else:
                data.extend([item.yPos, PixelInfo.roundXPos(item.xPos)])
            if item.kind.isFollowing():
                data.append(item.rangeValue)
            else:
                data.append(self.helper.encodeRangeKind(item.range))
            data.extend([item.moveOffset, self.helper.encodeEnemyKind(item.kind, item.timeResist)])
        data.append(self.end)
        return data

    def encodeLasers(self, items: list):
        data = list()
        for item in items:
            rightColumn = item.column + item.width - 1
            data.extend([item.column, rightColumn, item.row, item.mask,
                         self.helper.encodeLaserKind(item.kind, item.timeResist)])
        data.append(self.end)
        return data

    def encodeStates(self, items: list):
        data = list()
        for item in items:
            data.extend([self.helper.encodeBool(item.on), self.helper.encodeBool(item.timeResist)])
        data.append(self.end)
        return data

    def encodeActivableItems(self, levelInfo: LevelInfo):
        data = list()
        for item in levelInfo.forceFields:
            if item.methodKind == ActivationMethodKind.Time:
                maskOrState = item.mask
            else:
                maskOrState = item.state
            active = not levelInfo.isForceFieldActive(item, 0, 0)
            data.extend([item.column, item.row, item.height, self.helper.encodeBool(active), maskOrState,
                         self.helper.encodeActivableItemKind(
                             item.methodKind, item.invertedActivation, item.timeResist, ActivableItemKind.ForceField)])
        for item in levelInfo.disappearingPlatforms:
            if item.methodKind == ActivationMethodKind.Time:
                maskOrState = item.mask
            else:
                maskOrState = item.state
            active = not levelInfo.isDisappearingPlatformActive(item, 0, 0)
            data.extend([item.column, item.row, item.width, self.helper.encodeBool(active), maskOrState,
                         self.helper.encodeActivableItemKind(
                             item.methodKind, item.invertedActivation, item.timeResist,
                             ActivableItemKind.DisappearingPlatform)])
        for item in levelInfo.disappearingLadders:
            if item.methodKind == ActivationMethodKind.Time:
                maskOrState = item.mask
            else:
                maskOrState = item.state
            active = not levelInfo.isDisappearingLadderActive(item, 0, 0)
            data.extend([item.column, item.row, item.height, self.helper.encodeBool(active), maskOrState,
                         self.helper.encodeActivableItemKind(
                             item.methodKind, item.invertedActivation, item.timeResist,
                             ActivableItemKind.DisappearingLadder)])
        data.append(self.end)
        return data

    def encodeButtons(self, items: list):
        data = list()
        for item in items:
            kind = self.helper.encodeButtonKind(item.kind, item.timeResist)
            if item.kind.isTimer():
                data.extend([item.column, item.row, item.state, kind, item.durationFrames()])
            else:
                data.extend([item.column, item.row, item.state, kind])
        data.append(self.end)
        return data

    def encodeHourglasses(self, items: list):
        data = list()
        for item in items:
            data.extend([item.column, item.row, self.helper.encodeBool(item.timeResist)])
        data.append(self.end)
        return data

    def encodePortals(self, items: list):
        data = list()
        for item in items:
            if item.kind.isVertical():
                data.extend([item.column, item.row, item.height, self.helper.encodePortalKind(item.kind)])
            else:
                data.extend([item.column, item.row, item.width, self.helper.encodePortalKind(item.kind)])
        data.append(self.end)
        return data
