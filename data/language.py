from enum import Enum

from commands.commandKind import CommandKind
from data.enums import BoardColor, BoardItemKind, EnemyKind, LaserKind, ButtonKind, ActivationMethodKind, \
    PortalEntryKind, ProblemKind


class LanguageKind(int, Enum):
    en = 0
    pl = 1
    es = 2


class Language:

    MainTitle = {
        LanguageKind.en: 'Time Wizard level editor',
        LanguageKind.pl: 'Time Wizard edytor poziomów',
        LanguageKind.es: 'Editor de niveles de Time Wizard'
    }

    Untitled = {
        LanguageKind.en: 'Untitled',
        LanguageKind.pl: 'Bez nazwy',
        LanguageKind.es: 'Sin título'
    }

    Warning = {
        LanguageKind.en: 'Warning!',
        LanguageKind.pl: 'Uwaga!',
        LanguageKind.es: '¡Atención!'
    }

    LevelNotSaved = {
        LanguageKind.en: 'The level is not saved. You will loss your work. Are you sure?',
        LanguageKind.pl: 'Poziom nie został zapisany. Utracisz swoją pracę. Czy jesteś pewien?',
        LanguageKind.es: 'El nivel no está guardado. Perderá lo trabajado. ¿Está seguro?'
    }

    ThereAreProblemsExport = {
        LanguageKind.en: 'Problems has been detected in the level. Do you really want to export it?',
        LanguageKind.pl: 'Wykryto problemy w poziomie. Czy naprawdę chcesz go wyeksportować?',
        LanguageKind.es: 'Se han detectado problemas en el nivel. ¿Desea realmente exportarlo?'
    }

    ThereAreProblemsTest = {
        LanguageKind.en: 'Problems has been detected in the level. Please fix them before you run the test.',
        LanguageKind.pl: 'Wykryto problemy w poziomie. Proszę popraw je zanim uruchomisz test.',
        LanguageKind.es: 'Se han detectado problemas en el nivel. Por favor, soluciónelos antes de ejecutar la prueba.'
    }

    Yes = {
        LanguageKind.en: 'Yes',
        LanguageKind.pl: 'Tak',
        LanguageKind.es: 'Sí'
    }

    No = {
        LanguageKind.en: 'No',
        LanguageKind.pl: 'Nie',
        LanguageKind.es: 'No'
    }

    JsonFiles = {
        LanguageKind.en: 'JSON files (*.json)',
        LanguageKind.pl: 'Pliki JSON (*.json)',
        LanguageKind.es: 'Archivos JSON (*.json)'
    }

    AsmFiles = {
        LanguageKind.en: 'Asm files (*.asm)',
        LanguageKind.pl: 'Pliki asm (*.asm)',
        LanguageKind.es: 'Archivos asm (*.asm)'
    }

    Problems = {
        LanguageKind.en: 'Problems',
        LanguageKind.pl: 'Problemy',
        LanguageKind.es: 'Problemas'
    }

    Undo = {
        LanguageKind.en: 'Undo',
        LanguageKind.pl: 'Cofnij',
        LanguageKind.es: 'Deshacer'
    }

    Redo = {
        LanguageKind.en: 'Redo',
        LanguageKind.pl: 'Ponów',
        LanguageKind.es: 'Rehacer'
    }

    New = {
        LanguageKind.en: 'New',
        LanguageKind.pl: 'Nowy',
        LanguageKind.es: 'Nuevo'
    }

    Open = {
        LanguageKind.en: 'Open...',
        LanguageKind.pl: 'Otwórz...',
        LanguageKind.es: 'Abrir...'
    }

    Save = {
        LanguageKind.en: 'Save',
        LanguageKind.pl: 'Zapisz',
        LanguageKind.es: 'Guardar'
    }

    SaveAs = {
        LanguageKind.en: 'Save as...',
        LanguageKind.pl: 'Zapisz jako...',
        LanguageKind.es: 'Guardar como...'
    }

    Import = {
        LanguageKind.en: 'Import',
        LanguageKind.pl: 'Importuj',
        LanguageKind.es: 'Importar '
    }

    Export = {
        LanguageKind.en: 'Export',
        LanguageKind.pl: 'Exportuj',
        LanguageKind.es: 'Exportar '
    }

    Exit = {
        LanguageKind.en: 'Exit',
        LanguageKind.pl: 'Wyjdź',
        LanguageKind.es: 'Salida'
    }

    Cut = {
        LanguageKind.en: 'Cut',
        LanguageKind.pl: 'Wytnij',
        LanguageKind.es: 'Cortar'
    }

    Copy = {
        LanguageKind.en: 'Copy',
        LanguageKind.pl: 'Kopiuj',
        LanguageKind.es: 'Copiar'
    }

    Paste = {
        LanguageKind.en: 'Paste',
        LanguageKind.pl: 'Wklej',
        LanguageKind.es: 'Pegar'
    }

    ZoomIn = {
        LanguageKind.en: 'Zoom in',
        LanguageKind.pl: 'Powiększ',
        LanguageKind.es: 'Ampliar'
    }

    ZoomOut = {
        LanguageKind.en: 'Zoom out',
        LanguageKind.pl: 'Pomniejsz',
        LanguageKind.es: 'Alejar'
    }

    General = {
        LanguageKind.en: 'General',
        LanguageKind.pl: 'Ogólne',
        LanguageKind.es: 'General'
    }

    LevelPortalIn = {
        LanguageKind.en: 'Level portal in',
        LanguageKind.pl: 'Portal do poziomu',
        LanguageKind.es: 'Portal de ingreso al nivel'
    }

    LevelPortalOut = {
        LanguageKind.en: 'Level portal out',
        LanguageKind.pl: 'Portal z poziomu',
        LanguageKind.es: 'Portal de salida del nivel'
    }

    BoardItems = {
        LanguageKind.en: 'Board items',
        LanguageKind.pl: 'Elementy planszy',
        LanguageKind.es: 'Elementos del tablero'
    }

    MovingPlatforms = {
        LanguageKind.en: 'Moving platforms',
        LanguageKind.pl: 'Ruchome platformy',
        LanguageKind.es: 'Plataformas móviles'
    }

    Enemies = {
        LanguageKind.en: 'Enemies',
        LanguageKind.pl: 'Wrogowie',
        LanguageKind.es: 'Enemigos'
    }

    Lasers = {
        LanguageKind.en: 'Lasers',
        LanguageKind.pl: 'Lasery',
        LanguageKind.es: 'Láseres'
    }

    States = {
        LanguageKind.en: 'States',
        LanguageKind.pl: 'Stany',
        LanguageKind.es: 'Estados'
    }

    ForceFields = {
        LanguageKind.en: 'Force fields',
        LanguageKind.pl: 'Pola siłowe',
        LanguageKind.es: 'Campos de fuerza'
    }

    DisappearingPlatforms = {
        LanguageKind.en: 'Disappearing platforms',
        LanguageKind.pl: 'Znikające platformy',
        LanguageKind.es: 'Plataformas que desaparecen'
    }

    DisappearingLadders = {
        LanguageKind.en: 'Disappearing ladders',
        LanguageKind.pl: 'Znikające drabiny',
        LanguageKind.es: 'Escaleras que desaparecen'
    }

    Buttons = {
        LanguageKind.en: 'Buttons',
        LanguageKind.pl: 'Przyciski',
        LanguageKind.es: 'Botones'
    }

    Hourglasses = {
        LanguageKind.en: 'Hourglasses',
        LanguageKind.pl: 'Klepsydry',
        LanguageKind.es: 'Relojes de arena'
    }

    Portals = {
        LanguageKind.en: 'Portals',
        LanguageKind.pl: 'Portale',
        LanguageKind.es: 'Portales'
    }

    About = {
        LanguageKind.en: 'About',
        LanguageKind.pl: 'O programie',
        LanguageKind.es: 'Acerca de'
    }

    File = {
        LanguageKind.en: 'File',
        LanguageKind.pl: 'Plik',
        LanguageKind.es: 'Archivo'
    }

    Edit = {
        LanguageKind.en: 'Edit',
        LanguageKind.pl: 'Edytuj',
        LanguageKind.es: 'Editar'
    }

    View = {
        LanguageKind.en: 'View',
        LanguageKind.pl: 'Widok',
        LanguageKind.es: 'Ver'
    }

    Language = {
        LanguageKind.en: 'Language',
        LanguageKind.pl: 'Język (Language)',
        LanguageKind.es: 'Idioma (Language)'
    }

    Help = {
        LanguageKind.en: 'Help',
        LanguageKind.pl: 'Pomoc',
        LanguageKind.es: 'Ayuda'
    }

    Color = {
        LanguageKind.en: 'Color',
        LanguageKind.pl: 'Kolor',
        LanguageKind.es: 'Color'
    }

    Columns = {
        LanguageKind.en: 'Columns',
        LanguageKind.pl: 'Kolumny',
        LanguageKind.es: 'Columnas'
    }

    TimeSec = {
        LanguageKind.en: 'Time [sec]',
        LanguageKind.pl: 'Czas [sek]',
        LanguageKind.es: 'Tiempo [seg]'
    }

    ActionForAllHourglasses = {
        LanguageKind.en: 'Action at the end',
        LanguageKind.pl: 'Akcja na końcu',
        LanguageKind.es: 'Acción al final'
    }

    StateForAllHourglasses = {
        LanguageKind.en: 'State for the action',
        LanguageKind.pl: 'Stan akcji',
        LanguageKind.es: 'Estado de la acción'
    }

    BoardColorEnum = {
        LanguageKind.en: {
            BoardColor.Gray: 'Gray',
            BoardColor.Brown: 'Brown',
            BoardColor.Green: 'Green',
            BoardColor.Blue: 'Blue',
            BoardColor.Violet: 'Violet',
            BoardColor.DarkBlue: 'Dark blue',
            BoardColor.Red: 'Red'
        },
        LanguageKind.pl: {
            BoardColor.Gray: 'Szary',
            BoardColor.Brown: 'Brązowy',
            BoardColor.Green: 'Zielony',
            BoardColor.Blue: 'Niebieski',
            BoardColor.Violet: 'Fioletowy',
            BoardColor.DarkBlue: 'Granatowy',
            BoardColor.Red: 'Czerwony'
        },
        LanguageKind.es: {
            BoardColor.Gray: 'Gris',
            BoardColor.Brown: 'Marrón',
            BoardColor.Green: 'Verde',
            BoardColor.Blue: 'Azul',
            BoardColor.Violet: 'Violeta',
            BoardColor.DarkBlue: 'Azul oscuro',
            BoardColor.Red: 'Rojo'
        },
    }

    Column = {
        LanguageKind.en: 'Column',
        LanguageKind.pl: 'Kolumna',
        LanguageKind.es: 'Columna'
    }

    Row = {
        LanguageKind.en: 'Row',
        LanguageKind.pl: 'Wiersz',
        LanguageKind.es: 'Fila'
    }

    Width = {
        LanguageKind.en: 'Width',
        LanguageKind.pl: 'Szerokość',
        LanguageKind.es: 'Ancho'
    }

    Height = {
        LanguageKind.en: 'Height',
        LanguageKind.pl: 'Wysokość',
        LanguageKind.es: 'Altura'
    }

    AddNew = {
        LanguageKind.en: 'Add new',
        LanguageKind.pl: 'Dodaj nowy',
        LanguageKind.es: 'Añadir nuevo'
    }

    MoveToTop = {
        LanguageKind.en: 'Move to top',
        LanguageKind.pl: 'Przesuń na wierzch',
        LanguageKind.es: 'Mover hasta arriba'
    }

    MoveUp = {
        LanguageKind.en: 'Move up',
        LanguageKind.pl: 'Przesuń w górę',
        LanguageKind.es: 'Subir'
    }

    MoveDown = {
        LanguageKind.en: 'Move down',
        LanguageKind.pl: 'Przesuń w dół',
        LanguageKind.es: 'Bajar'
    }

    MoveToBottom = {
        LanguageKind.en: 'Move to bottom',
        LanguageKind.pl: 'Przesuń na spód',
        LanguageKind.es: 'Mover al fondo'
    }

    Delete = {
        LanguageKind.en: 'Remove',
        LanguageKind.pl: 'Usuń',
        LanguageKind.es: 'Eliminar'
    }

    Kind = {
        LanguageKind.en: 'Kind',
        LanguageKind.pl: 'Rodzaj',
        LanguageKind.es: 'Tipo'
    }

    BoardItemKindEnum = {
        LanguageKind.en: {
            BoardItemKind.Void: 'Void',
            BoardItemKind.Air: 'Air',
            BoardItemKind.TimeBlockade: 'Time blockade',
            BoardItemKind.Wall1x1: 'Wall 1x1',
            BoardItemKind.BottomWall1x2: 'Bottom wall 1x2',
            BoardItemKind.TopWall1x2: 'Top wall 1x2',
            BoardItemKind.HorizontalWall: 'Horizontal wall',
            BoardItemKind.LongHorizontalWall: 'Long horizontal wall',
            BoardItemKind.VerticalWall: 'Vertical wall',
            BoardItemKind.LightPlatform: 'Light platform',
            BoardItemKind.Platform: 'Platform',
            BoardItemKind.Ladder: 'Ladder',
            BoardItemKind.LadderWithPlatform: 'Ladder with platform',
            BoardItemKind.BottomSpikes: 'Bottom spikes',
            BoardItemKind.TopSpikes: 'Top spikes',
            BoardItemKind.LeftSpikes: 'Left spikes',
            BoardItemKind.RightSpikes: 'Right spikes',
            BoardItemKind.Structure: 'Structure',
            BoardItemKind.BottomWall: 'Bottom wall',
            BoardItemKind.LeftWall: 'Left wall',
            BoardItemKind.RightWall: 'Right wall',
            BoardItemKind.TopWall: 'Top wall',
            BoardItemKind.LeftTopWall: 'Left-top wall',
            BoardItemKind.RightTopWall: 'Right-top wall',
            BoardItemKind.NarrowStructure: 'Narrow structure',
            BoardItemKind.TopWallWithCorners: 'Top wall with corners',
            BoardItemKind.Chain: 'Chain',
            BoardItemKind.Tile: 'Tile',
            BoardItemKind.Pattern4x4_1: 'Pattern 4x4 v1',
            BoardItemKind.Pattern4x4_2: 'Pattern 4x4 v2',
            BoardItemKind.Pattern4x4_3: 'Pattern 4x4 v3',
            BoardItemKind.Pattern3x3_1: 'Pattern 3x3 v1',
            BoardItemKind.Pattern3x3_2: 'Pattern 3x3 v2',
            BoardItemKind.Pattern3x3_3: 'Pattern 3x3 v3'
        },
        LanguageKind.pl: {
            BoardItemKind.Void: 'Pustka',
            BoardItemKind.Air: 'Powietrze',
            BoardItemKind.TimeBlockade: 'Blokada czasu',
            BoardItemKind.Wall1x1: 'Mur 1x1',
            BoardItemKind.BottomWall1x2: 'Mur z dołu 1x2',
            BoardItemKind.TopWall1x2: 'Mur z góry 1x2',
            BoardItemKind.HorizontalWall: 'Poziomy mur',
            BoardItemKind.LongHorizontalWall: 'Długi poziomy mur',
            BoardItemKind.VerticalWall: 'Pionowy mur',
            BoardItemKind.LightPlatform: 'Lekka platforma',
            BoardItemKind.Platform: 'Platforma',
            BoardItemKind.Ladder: 'Drabina',
            BoardItemKind.LadderWithPlatform: 'Drabina z platformą',
            BoardItemKind.BottomSpikes: 'Kolce z dołu',
            BoardItemKind.TopSpikes: 'Kolce z góry',
            BoardItemKind.LeftSpikes: 'Kolce z lewej',
            BoardItemKind.RightSpikes: 'Kolce z prawej',
            BoardItemKind.Structure: 'Struktura',
            BoardItemKind.BottomWall: 'Mur z dołu',
            BoardItemKind.LeftWall: 'Mur z lewej',
            BoardItemKind.RightWall: 'Mur z prawej',
            BoardItemKind.TopWall: 'Mur z góry',
            BoardItemKind.LeftTopWall: 'Mur lewy-górny',
            BoardItemKind.RightTopWall: 'Mur prawy-górny',
            BoardItemKind.NarrowStructure: 'Wąska struktura',
            BoardItemKind.TopWallWithCorners: 'Mur z góry z narożnikami',
            BoardItemKind.Chain: 'Łańcuch',
            BoardItemKind.Tile: 'Kafelek',
            BoardItemKind.Pattern4x4_1: 'Wzorek 4x4 v1',
            BoardItemKind.Pattern4x4_2: 'Wzorek 4x4 v2',
            BoardItemKind.Pattern4x4_3: 'Wzorek 4x4 v3',
            BoardItemKind.Pattern3x3_1: 'Wzorek 3x3 v1',
            BoardItemKind.Pattern3x3_2: 'Wzorek 3x3 v2',
            BoardItemKind.Pattern3x3_3: 'Wzorek 3x3 v3'
        },
        LanguageKind.es: {
            BoardItemKind.Void: 'Vacío',
            BoardItemKind.Air: 'Aire',
            BoardItemKind.TimeBlockade: 'Bloqueo del tiempo',
            BoardItemKind.Wall1x1: 'Pared 1x1',
            BoardItemKind.BottomWall1x2: 'Pared inferior 1x2',
            BoardItemKind.TopWall1x2: 'Pared superior 1x2',
            BoardItemKind.HorizontalWall: 'Pared horizontal',
            BoardItemKind.LongHorizontalWall: 'Pared horizontal larga',
            BoardItemKind.VerticalWall: 'Pared vertical',
            BoardItemKind.LightPlatform: 'Plataforma ligera',
            BoardItemKind.Platform: 'Plataforma',
            BoardItemKind.Ladder: 'Escalera',
            BoardItemKind.LadderWithPlatform: 'Escalera con plataforma',
            BoardItemKind.BottomSpikes: 'Púas inferiores',
            BoardItemKind.TopSpikes: 'Púas superiores',
            BoardItemKind.LeftSpikes: 'Púas a la izquierda',
            BoardItemKind.RightSpikes: 'Púas a la derecha',
            BoardItemKind.Structure: 'Estructura',
            BoardItemKind.BottomWall: 'Pared inferior',
            BoardItemKind.LeftWall: 'Pared izquierda',
            BoardItemKind.RightWall: 'Pared derecha',
            BoardItemKind.TopWall: 'Pared superior',
            BoardItemKind.LeftTopWall: 'Pared superior izquierda',
            BoardItemKind.RightTopWall: 'Pared superior derecha',
            BoardItemKind.NarrowStructure: 'Estructura estrecha',
            BoardItemKind.TopWallWithCorners: 'Pared superior con esquinas',
            BoardItemKind.Chain: 'Cadena',
            BoardItemKind.Tile: 'Mosaico',
            BoardItemKind.Pattern4x4_1: 'Patrón 4x4 v1',
            BoardItemKind.Pattern4x4_2: 'Patrón 4x4 v2',
            BoardItemKind.Pattern4x4_3: 'Patrón 4x4 v3',
            BoardItemKind.Pattern3x3_1: 'Patrón 3x3 v1',
            BoardItemKind.Pattern3x3_2: 'Patrón 3x3 v2',
            BoardItemKind.Pattern3x3_3: 'Patrón 3x3 v3'
        }
    }

    TimeResist = {
        LanguageKind.en: 'Time resist',
        LanguageKind.pl: 'Odporność czasowa',
        LanguageKind.es: 'Sensible al tiempo'
    }

    HorizontalPosition = {
        LanguageKind.en: 'Horizontal pos.',
        LanguageKind.pl: 'Poz. pozioma',
        LanguageKind.es: 'Pos. horizontal'
    }

    VerticalPosition = {
        LanguageKind.en: 'Vertical pos.',
        LanguageKind.pl: 'Poz. pionowa',
        LanguageKind.es: 'Pos. vertical'
    }

    Range = {
        LanguageKind.en: 'Range',
        LanguageKind.pl: 'Zasięg',
        LanguageKind.es: 'Rango'
    }

    Offset = {
        LanguageKind.en: 'Offset',
        LanguageKind.pl: 'Przesunięcie',
        LanguageKind.es: 'Desplazamiento'
    }

    EnemyKindEnum = {
        LanguageKind.en: {
            EnemyKind.Horizontal: 'Horizontal',
            EnemyKind.Vertical: 'Vertical',
            EnemyKind.FollowingHorizontal: 'Following horizontal',
            EnemyKind.FollowingVertical: 'Following vertical'
        },
        LanguageKind.pl: {
            EnemyKind.Horizontal: 'Poziomy',
            EnemyKind.Vertical: 'Pionowy',
            EnemyKind.FollowingHorizontal: 'Poziomy śledzący',
            EnemyKind.FollowingVertical: 'Pionowy śledzący'
        },
        LanguageKind.es: {
            EnemyKind.Horizontal: 'Horizontal',
            EnemyKind.Vertical: 'Vertical',
            EnemyKind.FollowingHorizontal: 'Siguiente horizntal',
            EnemyKind.FollowingVertical: 'Siguiente vertical'
        }
    }

    TimePattern = {
        LanguageKind.en: 'Time pattern',
        LanguageKind.pl: 'Wzorzec czasowy',
        LanguageKind.es: 'Patrón de tiempo'
    }

    LaserKindEnum = {
        LanguageKind.en: {
            LaserKind.LeftToRight: 'Left to right',
            LaserKind.RightToLeft: 'Right to left'
        },
        LanguageKind.pl: {
            LaserKind.LeftToRight: 'Z lewa na prawo',
            LaserKind.RightToLeft: 'Z prawa na lewo'
        },
        LanguageKind.es: {
            LaserKind.LeftToRight: 'Izquierda a derecha',
            LaserKind.RightToLeft: 'Derecha a izquierda'
        }
    }

    TurnedOn = {
        LanguageKind.en: 'Turned on',
        LanguageKind.pl: 'Włączony',
        LanguageKind.es: 'Encendido'
    }

    ButtonKindEnum = {
        LanguageKind.en: {
            ButtonKind.TurnsOn: 'Turns on',
            ButtonKind.TurnsOff: 'Turns off',
            ButtonKind.TurnsOnOff: 'Turns on/off',
            ButtonKind.TurnsOnTimer: 'Turns on w. timer',
            ButtonKind.TurnsOffTimer: 'Turns off w. timer'
        },
        LanguageKind.pl: {
            ButtonKind.TurnsOn: 'Włącza',
            ButtonKind.TurnsOff: 'Wyłącza',
            ButtonKind.TurnsOnOff: 'Włącza/wyłącza',
            ButtonKind.TurnsOnTimer: 'Włącza czasowo',
            ButtonKind.TurnsOffTimer: 'Wyłącza czasowo'
        },
        LanguageKind.es: {
            ButtonKind.TurnsOn: 'Enciende',
            ButtonKind.TurnsOff: 'Apaga',
            ButtonKind.TurnsOnOff: 'Enciende/apaga',
            ButtonKind.TurnsOnTimer: 'Enciende a tiempo',
            ButtonKind.TurnsOffTimer: 'Apaga a tiempo'
        }
    }

    State = {
        LanguageKind.en: 'State',
        LanguageKind.pl: 'Stan',
        LanguageKind.es: 'Estado'
    }

    Duration = {
        LanguageKind.en: 'Time [sec]',
        LanguageKind.pl: 'Czas [sek]',
        LanguageKind.es: 'Tiempo [seg]'
    }

    ActivationMethod = {
        LanguageKind.en: 'Activation method',
        LanguageKind.pl: 'Metoda aktywacji',
        LanguageKind.es: 'Método de activación'
    }

    InvertedActivation = {
        LanguageKind.en: 'Inverted activation',
        LanguageKind.pl: 'Odwrócona aktywacja',
        LanguageKind.es: 'Activación invertida'
    }

    ActivationMethodKindEnum = {
        LanguageKind.en: {
            ActivationMethodKind.Time: 'Time',
            ActivationMethodKind.State: 'State'
        },
        LanguageKind.pl: {
            ActivationMethodKind.Time: 'Czas',
            ActivationMethodKind.State: 'Stan'
        },
        LanguageKind.es: {
            ActivationMethodKind.Time: 'Tiempo',
            ActivationMethodKind.State: 'Estado'
        }
    }

    Entry = {
        LanguageKind.en: 'Entry',
        LanguageKind.pl: 'Wejście',
        LanguageKind.es: 'Entrada'
    }

    PortalEntryKindEnum = {
        LanguageKind.en: {
            PortalEntryKind.LeftSide: 'Left side',
            PortalEntryKind.RightSide: 'Right side',
            PortalEntryKind.BothSides: 'Both sides',
            PortalEntryKind.TopSide: 'Top side',
            PortalEntryKind.BottomSide: 'Bottom side'
        },
        LanguageKind.pl: {
            PortalEntryKind.LeftSide: 'Lewa strona',
            PortalEntryKind.RightSide: 'Prawa strona',
            PortalEntryKind.BothSides: 'Obydwie strony',
            PortalEntryKind.TopSide: 'Górna strona',
            PortalEntryKind.BottomSide: 'Dolna strona'
        },
        LanguageKind.es: {
            PortalEntryKind.LeftSide: 'Lado izquierdo',
            PortalEntryKind.RightSide: 'Lado derecho',
            PortalEntryKind.BothSides: 'Ambos lados',
            PortalEntryKind.TopSide: 'Lado superior',
            PortalEntryKind.BottomSide: 'Lado inferior'
        }
    }

    Bool = {
        LanguageKind.en: {
            False: 'False',
            True: 'True'
        },
        LanguageKind.pl: {
            False: 'Fałsz',
            True: 'Prawda'
        },
        LanguageKind.es: {
            False: 'Falso',
            True: 'Verdadero'
        }
    }

    ProblemList = {
        LanguageKind.en: 'Problem list',
        LanguageKind.pl: 'Lista problemów',
        LanguageKind.es: 'Lista de problemas'
    }

    ProblemKindEnum = {
        LanguageKind.en: {
            ProblemKind.OutsideBoard: 'Object is outside the board',
            ProblemKind.LackOfHourglasses: 'Lack of hourglasses',
            ProblemKind.PortalWithoutPair: 'Portal without a pair',
            ProblemKind.PortalPairOrientation: 'Pair of portals have different orientation',
            ProblemKind.ObjectOverlap: 'Object overlaps another',
            ProblemKind.StateDoesNotExist: 'State does not exist on the list',
            ProblemKind.TooManyMovingPlatforms: 'Number of moving platforms exceeds 8',
            ProblemKind.TooManyEnemies: 'Number of enemies exceeds 4',
            ProblemKind.TooManyLasers: 'Number of lasers exceeds 8',
            ProblemKind.TooManyActivableItems: 'Number of force fields + disappearing platforms + disappearing ladders exceeds 16',
            ProblemKind.TooManyButtons: 'Number of buttons exceeds 8',
            ProblemKind.TooManyStates: 'Number of states exceeds 8',
            ProblemKind.TooManyHourglasses: 'Number of hourglasses exceeds 32',
            ProblemKind.TooManyPortals: 'Number of portals exceeds 16',
            ProblemKind.InvalidTimeResist: 'Object and connected state have different time resist value',
            ProblemKind.FollowingEnemyNotTimeResist: 'Following enemy is not time resist',
            ProblemKind.TimerButtonNotTimeResist: 'Timer button is not time resist',
            ProblemKind.StateUsedForAllHourglasses: 'Button controls state for action after all hourglasses are collected'
        },
        LanguageKind.pl: {
            ProblemKind.OutsideBoard: 'Obiekt jest poza planszą',
            ProblemKind.LackOfHourglasses: 'Brakuje klepsydr',
            ProblemKind.PortalWithoutPair: 'Portal nie ma pary',
            ProblemKind.PortalPairOrientation: 'Para portali ma różną orientację',
            ProblemKind.ObjectOverlap: 'Obiekt zachodzi na inny',
            ProblemKind.StateDoesNotExist: 'Stan nie istnieje na liście',
            ProblemKind.TooManyMovingPlatforms: 'Liczba ruchomych platform przekracza 8',
            ProblemKind.TooManyEnemies: 'Liczba wrogów przekracza 4',
            ProblemKind.TooManyLasers: 'Liczba laserów przekracza 8',
            ProblemKind.TooManyActivableItems: 'Liczba pól siłowych + znikających platform + znikających drabin przekracza 16',
            ProblemKind.TooManyButtons: 'Liczba przycisków przekracza 8',
            ProblemKind.TooManyStates: 'Liczba stanów przekracza 8',
            ProblemKind.TooManyHourglasses: 'Liczba klepsydr przekracza 32',
            ProblemKind.TooManyPortals: 'Liczba portali przekracza 16',
            ProblemKind.InvalidTimeResist: 'Obiekt i połączony z nim stan mają różną odporność czasową',
            ProblemKind.FollowingEnemyNotTimeResist: 'Śledzący wróg nie jest odporny na czas',
            ProblemKind.TimerButtonNotTimeResist: 'Czasowy przycisk nie jest odporny na czas',
            ProblemKind.StateUsedForAllHourglasses: 'Przycisk kontroluje stan dla akcji po zebraniu wszystkich klepsydr'
        },
        LanguageKind.es: {
            ProblemKind.OutsideBoard: 'El objeto está fuera del tablero',
            ProblemKind.LackOfHourglasses: 'Falta de relojes de arena',
            ProblemKind.PortalWithoutPair: 'Portal sin un par',
            ProblemKind.PortalPairOrientation: 'El par de portales tienen orientaciones diferentes',
            ProblemKind.ObjectOverlap: 'El objeto se superpone a otro',
            ProblemKind.StateDoesNotExist: 'El estado no figura en la lista',
            ProblemKind.TooManyMovingPlatforms: 'Número de plataformas móviles superior a 8',
            ProblemKind.TooManyEnemies: 'Número de enemigos superior a 4',
            ProblemKind.TooManyLasers: 'Número de láseres superior a 8',
            ProblemKind.TooManyActivableItems: 'Número de campos de fuerza + plataformas que desaparecen + escalera que desaperecen superior a 16',
            ProblemKind.TooManyButtons: 'Número de botones superior a 8',
            ProblemKind.TooManyStates: 'Número de estados superior a 8',
            ProblemKind.TooManyHourglasses: 'Número de relojes de arena es superior a 32',
            ProblemKind.TooManyPortals: 'Número de portales supera los 16',
            ProblemKind.InvalidTimeResist: 'El objeto y el estado conectado tienen diferentes resistencias temporales',
            ProblemKind.FollowingEnemyNotTimeResist: 'El enemigo siguiente no es el tiempo resiste',
            ProblemKind.TimerButtonNotTimeResist: 'El botón temporizado no es el tiempo resiste',
            ProblemKind.StateUsedForAllHourglasses: 'El botón controla el estado de la acción una vez recogidos todos los relojes de arena'
        }
    }

    MovingPlatform = {
        LanguageKind.en: 'Moving platform',
        LanguageKind.pl: 'Ruchoma platforma',
        LanguageKind.es: 'Plataforma móvil'
    }

    Enemy = {
        LanguageKind.en: 'Enemy',
        LanguageKind.pl: 'Wróg',
        LanguageKind.es: 'Enemigo'
    }

    Laser = {
        LanguageKind.en: 'Laser',
        LanguageKind.pl: 'Laser',
        LanguageKind.es: 'Láser'
    }

    ForceField = {
        LanguageKind.en: 'Force field',
        LanguageKind.pl: 'Pole siłowe',
        LanguageKind.es: 'Campo de fuerza'
    }

    DisappearingPlatform = {
        LanguageKind.en: 'Disappearing platform',
        LanguageKind.pl: 'Znikająca platforma',
        LanguageKind.es: 'Plataforma que desaparece'
    }

    DisappearingLadder = {
        LanguageKind.en: 'Disappearing ladder',
        LanguageKind.pl: 'Znikająca drabina',
        LanguageKind.es: 'Escalera que desaparece'
    }

    Button = {
        LanguageKind.en: 'Button',
        LanguageKind.pl: 'Przycisk',
        LanguageKind.es: 'Botón'
    }

    Hourglass = {
        LanguageKind.en: 'Hourglass',
        LanguageKind.pl: 'Klepsydra',
        LanguageKind.es: 'Reloj de arena'
    }

    Portal = {
        LanguageKind.en: 'Portal',
        LanguageKind.pl: 'Portal',
        LanguageKind.es: 'Portal'
    }

    CommandKindEnum = {
        LanguageKind.en: {
            CommandKind.GeneralBoardColor: 'board color',
            CommandKind.GeneralBoardColumns: 'board columns',
            CommandKind.GeneralLevelTime: 'level time',
            CommandKind.ActionAfterAllHourglasses: 'action at the end',
            CommandKind.StateAfterAllHourglasses: 'state of the action',
            CommandKind.LevelPortalInPosition: 'level portal in position',
            CommandKind.LevelPortalInSize: 'level portal in size',
            CommandKind.LevelPortalOutPosition: 'level portal out position',
            CommandKind.LevelPortalOutSize: 'level portal out size',
            CommandKind.BoardItemKind: 'board item kind',
            CommandKind.BoardItemPosition: 'board item position',
            CommandKind.BoardItemSize: 'board item size',
            CommandKind.MovingPlatformTimeResist: 'moving platform time resist',
            CommandKind.MovingPlatformPosition: 'moving platform position',
            CommandKind.MovingPlatformSize: 'moving platform size',
            CommandKind.MovingPlatformRange: 'moving platform range',
            CommandKind.MovingPlatformMoveOffset: 'moving platform offset',
            CommandKind.EnemyKind: 'enemy kind',
            CommandKind.EnemyTimeResist: 'enemy time resist',
            CommandKind.EnemyPosition: 'enemy position',
            CommandKind.EnemyRange: 'enemy range',
            CommandKind.EnemyRangeValue: 'enemy range',
            CommandKind.EnemyMoveOffset: 'enemy offset',
            CommandKind.LaserKind: 'laser kind',
            CommandKind.LaserTimeResist: 'laser time resist',
            CommandKind.LaserPosition: 'laser position',
            CommandKind.LaserSize: 'laser size',
            CommandKind.LaserMask: 'laser time pattern',
            CommandKind.StateTimeResist: 'state time resist',
            CommandKind.StateOn: 'state turned on',
            CommandKind.ButtonKind: 'button kind',
            CommandKind.ButtonTimeResist: 'button time resist',
            CommandKind.ButtonPosition: 'button position',
            CommandKind.ButtonState: 'button state',
            CommandKind.ButtonDuration: 'button duration',
            CommandKind.ForceFieldActivationMethod: 'force field activation method',
            CommandKind.ForceFieldInverted: 'force field inverted activation',
            CommandKind.ForceFieldTimeResist: 'force field time resist',
            CommandKind.ForceFieldPosition: 'force field position',
            CommandKind.ForceFieldSize: 'force field size',
            CommandKind.ForceFieldMask: 'force field time pattern',
            CommandKind.ForceFieldState: 'force field state',
            CommandKind.DisappearingPlatformActivationMethod: 'disappearing platform activation method',
            CommandKind.DisappearingPlatformInverted: 'disappearing platform inverted activation',
            CommandKind.DisappearingPlatformTimeResist: 'disappearing platform time resist',
            CommandKind.DisappearingPlatformPosition: 'disappearing platform position',
            CommandKind.DisappearingPlatformSize: 'disappearing platform size',
            CommandKind.DisappearingPlatformMask: 'disappearing platform time pattern',
            CommandKind.DisappearingPlatformState: 'disappearing platform state',
            CommandKind.DisappearingLadderActivationMethod: 'disappearing ladder activation method',
            CommandKind.DisappearingLadderInverted: 'disappearing ladder inverted activation',
            CommandKind.DisappearingLadderTimeResist: 'disappearing ladder time resist',
            CommandKind.DisappearingLadderPosition: 'disappearing ladder position',
            CommandKind.DisappearingLadderSize: 'disappearing ladder size',
            CommandKind.DisappearingLadderMask: 'disappearing ladder time pattern',
            CommandKind.DisappearingLadderState: 'disappearing ladder state',
            CommandKind.HourglassTimeResist: 'hourglass time resist',
            CommandKind.HourglassPosition: 'hourglass position',
            CommandKind.PortalKind: 'portal kind',
            CommandKind.PortalPosition: 'portal position',
            CommandKind.PortalSize: 'portal size',
            CommandKind.ListAddItem: 'add new item',
            CommandKind.ListRemoveItem: 'remove item',
            CommandKind.ListMoveItem: 'move item',
            CommandKind.GroupTimeResist: 'time resist'
        },
        LanguageKind.pl: {
            CommandKind.GeneralBoardColor: 'kolor planszy',
            CommandKind.GeneralBoardColumns: 'kolumny planszy',
            CommandKind.GeneralLevelTime: 'czas poziomu',
            CommandKind.ActionAfterAllHourglasses: 'akcja na końcu',
            CommandKind.StateAfterAllHourglasses: 'stan akcji',
            CommandKind.LevelPortalInPosition: 'pozycja portalu do poziomu',
            CommandKind.LevelPortalInSize: 'rozmiar portalu do poziomu',
            CommandKind.LevelPortalOutPosition: 'pozycja portalu z poziomu',
            CommandKind.LevelPortalOutSize: 'rozmiar portalu z poziomu',
            CommandKind.BoardItemKind: 'rodzaj elementu planszy',
            CommandKind.BoardItemPosition: 'pozycja elementu planszy',
            CommandKind.BoardItemSize: 'rozmiar elementu planszy',
            CommandKind.MovingPlatformTimeResist: 'odporność czasowa ruchomej platformy',
            CommandKind.MovingPlatformPosition: 'pozycja ruchomej platformy',
            CommandKind.MovingPlatformSize: 'szerokość ruchomej platformy',
            CommandKind.MovingPlatformRange: 'zasięg ruchomej platformy',
            CommandKind.MovingPlatformMoveOffset: 'przesunięcie ruchomej platformy',
            CommandKind.EnemyKind: 'rodzaj wroga',
            CommandKind.EnemyTimeResist: 'odporność czasowa wroga',
            CommandKind.EnemyPosition: 'pozycja wroga',
            CommandKind.EnemyRange: 'zasięg wroga',
            CommandKind.EnemyRangeValue: 'zasięg wroga',
            CommandKind.EnemyMoveOffset: 'przesunięcie wroga',
            CommandKind.LaserKind: 'rodzaj lasera',
            CommandKind.LaserTimeResist: 'odporność czasowa lasera',
            CommandKind.LaserPosition: 'pozycja lasera',
            CommandKind.LaserSize: 'rozmiar lasera',
            CommandKind.LaserMask: 'wzorzec czasowy lasera',
            CommandKind.StateTimeResist: 'odporność czasowa stanu',
            CommandKind.StateOn: 'włączenie stanu',
            CommandKind.ButtonKind: 'rodzaj przycisku',
            CommandKind.ButtonTimeResist: 'odporność czasowa przycisku',
            CommandKind.ButtonPosition: 'pozycja przycisku',
            CommandKind.ButtonState: 'stan przycisku',
            CommandKind.ButtonDuration: 'czas przycisku',
            CommandKind.ForceFieldActivationMethod: 'metoda aktywacji pola siłowego',
            CommandKind.ForceFieldInverted: 'odwrócona aktywacja pola siłowego',
            CommandKind.ForceFieldTimeResist: 'odporność czasowa pola siłowego',
            CommandKind.ForceFieldPosition: 'pozycja pola siłowego',
            CommandKind.ForceFieldSize: 'rozmiar pola siłowego',
            CommandKind.ForceFieldMask: 'wzorzec czasowy pola siłowego',
            CommandKind.ForceFieldState: 'stan pola siłowego',
            CommandKind.DisappearingPlatformActivationMethod: 'metoda aktywacji znikającej platformy',
            CommandKind.DisappearingPlatformInverted: 'odwrócona aktywacja znikającej platformy',
            CommandKind.DisappearingPlatformTimeResist: 'odporność czasowa znikającej platformy',
            CommandKind.DisappearingPlatformPosition: 'pozycja znikającej platformy',
            CommandKind.DisappearingPlatformSize: 'rozmiar znikającej platformy',
            CommandKind.DisappearingPlatformMask: 'wzorzec czasowy znikającej platformy',
            CommandKind.DisappearingPlatformState: 'stan znikającej platformy',
            CommandKind.DisappearingLadderActivationMethod: 'metoda aktywacji znikającej drabiny',
            CommandKind.DisappearingLadderInverted: 'odwrócona aktywacja znikającej drabiny',
            CommandKind.DisappearingLadderTimeResist: 'odporność czasowa znikającej drabiny',
            CommandKind.DisappearingLadderPosition: 'pozycja znikającej drabiny',
            CommandKind.DisappearingLadderSize: 'rozmiar znikającej drabiny',
            CommandKind.DisappearingLadderMask: 'wzorzec czasowy znikającej drabiny',
            CommandKind.DisappearingLadderState: 'stan znikającej drabiny',
            CommandKind.HourglassTimeResist: 'odporność czasowa klepsydry',
            CommandKind.HourglassPosition: 'pozycja klepsydry',
            CommandKind.PortalKind: 'rodzaj portalu',
            CommandKind.PortalPosition: 'pozycja portalu',
            CommandKind.PortalSize: 'rozmiar portalu',
            CommandKind.ListAddItem: 'dodanie nowego obiektu',
            CommandKind.ListRemoveItem: 'usunięcie obiektu',
            CommandKind.ListMoveItem: 'przeniesienie obiektu',
            CommandKind.GroupTimeResist: 'odporność czasowa'
        },
        LanguageKind.es: {
            CommandKind.GeneralBoardColor: 'color del tablero',
            CommandKind.GeneralBoardColumns: 'columnas del tablero',
            CommandKind.GeneralLevelTime: 'tiempo del nivel',
            CommandKind.ActionAfterAllHourglasses: 'acción al final',
            CommandKind.StateAfterAllHourglasses: 'estado de la acción',
            CommandKind.LevelPortalInPosition: 'portal de nivel en posición',
            CommandKind.LevelPortalInSize: 'portal de nivel en tamaño',
            CommandKind.LevelPortalOutPosition: 'portal de nivel fuera de posición',
            CommandKind.LevelPortalOutSize: 'portal de nivel fuera de tamaño',
            CommandKind.BoardItemKind: 'tipo de elemento',
            CommandKind.BoardItemPosition: 'posición de elemento del tablero',
            CommandKind.BoardItemSize: 'tamaño de elemento',
            CommandKind.MovingPlatformTimeResist: 'plataforma móvil sensible al tiempo',
            CommandKind.MovingPlatformPosition: 'posición de la plataforma móvil',
            CommandKind.MovingPlatformSize: 'tamaño de la plataforma móvil',
            CommandKind.MovingPlatformRange: 'rango de plataforma móvil',
            CommandKind.MovingPlatformMoveOffset: 'desplazamiento de la plataforma móvil',
            CommandKind.EnemyKind: 'tipo de enemigo',
            CommandKind.EnemyTimeResist: 'sensibilidad al tiempo del enemigo',
            CommandKind.EnemyPosition: 'posición del enemigo',
            CommandKind.EnemyRange: 'rango del enemigo',
            CommandKind.EnemyRangeValue: 'rango del enemigo',
            CommandKind.EnemyMoveOffset: 'desplazamiento del enemigo',
            CommandKind.LaserKind: 'tipo de láser',
            CommandKind.LaserTimeResist: 'sensibilidad al tiempo del láser',
            CommandKind.LaserPosition: 'posición del láser',
            CommandKind.LaserSize: 'tamaño del láser',
            CommandKind.LaserMask: 'patrón de tiempo del láser',
            CommandKind.StateTimeResist: 'sensibilidad al tiempo del estado',
            CommandKind.StateOn: 'estado activado',
            CommandKind.ButtonKind: 'tipo de botón',
            CommandKind.ButtonTimeResist: 'sensibilidad al tiempo del botón',
            CommandKind.ButtonPosition: 'posición del botón',
            CommandKind.ButtonState: 'estado del botón',
            CommandKind.ButtonDuration: 'duración del botón',
            CommandKind.ForceFieldActivationMethod: 'método de activación del campo de fuerza',
            CommandKind.ForceFieldInverted: 'activación invertida del campo de fuerza',
            CommandKind.ForceFieldTimeResist: 'sensibilidad al tiempo del campo de fuerza',
            CommandKind.ForceFieldPosition: 'posición del campo de fuerza',
            CommandKind.ForceFieldSize: 'tamaño del campo de fuerza',
            CommandKind.ForceFieldMask: 'patrón de tiempo del campo de fuerza',
            CommandKind.ForceFieldState: 'estado del campo de fuerza',
            CommandKind.DisappearingPlatformActivationMethod: 'método de activación de la plataforma que desaparece',
            CommandKind.DisappearingPlatformInverted: 'activación invertida de la plataforma que desaparece',
            CommandKind.DisappearingPlatformTimeResist: 'sensibilidad al tiempo de la plataforma que desaparece',
            CommandKind.DisappearingPlatformPosition: 'posición de la plataforma que desaparece',
            CommandKind.DisappearingPlatformSize: 'tamaño de la plataforma que desaparece',
            CommandKind.DisappearingPlatformMask: 'patrón de tiempo de la plataforma que desaparece',
            CommandKind.DisappearingPlatformState: 'estado de la plataforma que desaparece',
            CommandKind.DisappearingLadderActivationMethod: 'método de activación de la escalera que desaparece',
            CommandKind.DisappearingLadderInverted: 'activación invertida de la escalear que desaparece',
            CommandKind.DisappearingLadderTimeResist: 'sensibilidad al tiempo de la escalera que desaparece',
            CommandKind.DisappearingLadderPosition: 'posición de la escalera que desaparece',
            CommandKind.DisappearingLadderSize: 'tamaño de la escalera que desaparece',
            CommandKind.DisappearingLadderMask: 'patrón de tiempo de la escalera que desaparece',
            CommandKind.DisappearingLadderState: 'estado de la escalera que desaparece',
            CommandKind.HourglassTimeResist: 'sensibilidad al tiempo del reloj de arena',
            CommandKind.HourglassPosition: 'posición del reloj de arena',
            CommandKind.PortalKind: 'tipo de portal',
            CommandKind.PortalPosition: 'posición del portal',
            CommandKind.PortalSize: 'tamaño del portal',
            CommandKind.ListAddItem: 'añadir nuevo elemento',
            CommandKind.ListRemoveItem: 'eliminar elemento',
            CommandKind.ListMoveItem: 'mover elemento',
            CommandKind.GroupTimeResist: 'sensibilidad al tiempo'
        }
    }

    Error = {
        LanguageKind.en: 'Error!',
        LanguageKind.pl: 'Błąd!',
        LanguageKind.es: '¡Error!'
    }

    ImportError = {
        LanguageKind.en: 'Error during file import.',
        LanguageKind.pl: 'Błąd podczas importowania pliku.',
        LanguageKind.es: 'Error durante la importación del archivo.'
    }

    ExportError = {
        LanguageKind.en: 'Error during file export.',
        LanguageKind.pl: 'Błąd podczas eksportowania pliku.',
        LanguageKind.es: 'Error durante la exportación del archivo.'
    }

    LoadError = {
        LanguageKind.en: 'Error during file loading.',
        LanguageKind.pl: 'Błąd podczas ładowania pliku.',
        LanguageKind.es: 'Error durante la carga del archivo.'
    }

    SaveError = {
        LanguageKind.en: 'Error during file saving.',
        LanguageKind.pl: 'Błąd podczas zapisywania pliku.',
        LanguageKind.es: 'Error al guardar el archivo.'
    }

    TestError = {
        LanguageKind.en: 'Error during level testing.',
        LanguageKind.pl: 'Błąd podczas testowania poziomu.',
        LanguageKind.es: 'Error durante la prueba del nivel.'
    }

    Close = {
        LanguageKind.en: 'Close',
        LanguageKind.pl: 'Zamknij',
        LanguageKind.es: 'Cerrar'
    }

    Test = {
        LanguageKind.en: 'Test',
        LanguageKind.pl: 'Test',
        LanguageKind.es: 'Probar'
    }

    RunTest = {
        LanguageKind.en: 'Test the level',
        LanguageKind.pl: 'Przetestuj poziom',
        LanguageKind.es: 'Probar el nivel'
    }

    AddArea = {
        LanguageKind.en: 'Add area',
        LanguageKind.pl: 'Dodaj obszar',
        LanguageKind.es: 'Añadir zona'
    }

    AddWall = {
        LanguageKind.en: 'Add wall',
        LanguageKind.pl: 'Dodaj mur',
        LanguageKind.es: 'Añadir pared'
    }

    AddPlatform = {
        LanguageKind.en: 'Add platform',
        LanguageKind.pl: 'Dodaj platformę',
        LanguageKind.es: 'Añadir plataforma'
    }

    AddLadder = {
        LanguageKind.en: 'Add ladder',
        LanguageKind.pl: 'Dodaj drabinę',
        LanguageKind.es: 'Añadir escalera'
    }

    AddLadderWithPlatform = {
        LanguageKind.en: 'Add ladder with platform',
        LanguageKind.pl: 'Dodaj drabinę z platformą',
        LanguageKind.es: 'Añadir escalera con plataforma'
    }

    AddSpikes = {
        LanguageKind.en: 'Add spikes',
        LanguageKind.pl: 'Dodaj kolce',
        LanguageKind.es: 'Añadir púas'
    }

    AddDecoration = {
        LanguageKind.en: 'Add decoration',
        LanguageKind.pl: 'Dodaj dekorację',
        LanguageKind.es: 'Añadir decoración'
    }

    AddMovingPlatform = {
        LanguageKind.en: 'Add moving platform',
        LanguageKind.pl: 'Dodaj ruchomą platformę',
        LanguageKind.es: 'Añadir plataforma móvil'
    }

    AddEnemy = {
        LanguageKind.en: 'Add enemy',
        LanguageKind.pl: 'Dodaj wroga',
        LanguageKind.es: 'Añadir enemigo'
    }

    AddLaser = {
        LanguageKind.en: 'Add laser',
        LanguageKind.pl: 'Dodaj laser',
        LanguageKind.es: 'Añadir láser'
    }

    AddButton = {
        LanguageKind.en: 'Add button',
        LanguageKind.pl: 'Dodaj przycisk',
        LanguageKind.es: 'Añadir botón'
    }

    AddForceField = {
        LanguageKind.en: 'Add force field',
        LanguageKind.pl: 'Dodaj pole siłowe',
        LanguageKind.es: 'Añadir campo de fuerza'
    }

    AddDisappearingPlatform = {
        LanguageKind.en: 'Add disappearing platform',
        LanguageKind.pl: 'Dodaj znikającą platformę',
        LanguageKind.es: 'Añadir plataforma que desaparece'
    }

    AddDisappearingLadder = {
        LanguageKind.en: 'Add disappearing ladder',
        LanguageKind.pl: 'Dodaj znikającą drabinę',
        LanguageKind.es: 'Añadir escalera que desaparece'
    }

    AddPortal = {
        LanguageKind.en: 'Add portal',
        LanguageKind.pl: 'Dodaj portal',
        LanguageKind.es: 'Añadir portall'
    }

    AddHourglass = {
        LanguageKind.en: 'Add hourglass',
        LanguageKind.pl: 'Dodaj klepsydrę',
        LanguageKind.es: 'Añadir reloj de arena'
    }

    def __init__(self, lang: LanguageKind):
        self.lang = lang

    def mainTitle(self):
        return self.MainTitle[self.lang]

    def untitled(self):
        return self.Untitled[self.lang]

    def warning(self):
        return self.Warning[self.lang]

    def levelNotSaved(self):
        return self.LevelNotSaved[self.lang]

    def thereAreProblemsExport(self):
        return self.ThereAreProblemsExport[self.lang]

    def thereAreProblemsTest(self):
        return self.ThereAreProblemsTest[self.lang]

    def yes(self):
        return self.Yes[self.lang]

    def no(self):
        return self.No[self.lang]

    def jsonFiles(self):
        return self.JsonFiles[self.lang]

    def asmFiles(self):
        return self.AsmFiles[self.lang]

    def problems(self):
        return self.Problems[self.lang]

    def undo(self):
        return self.Undo[self.lang]

    def redo(self):
        return self.Redo[self.lang]

    def new(self):
        return self.New[self.lang]

    def open(self):
        return self.Open[self.lang]

    def save(self):
        return self.Save[self.lang]

    def saveAs(self):
        return self.SaveAs[self.lang]

    def import_(self):
        return self.Import[self.lang]

    def export(self):
        return self.Export[self.lang]

    def exit(self):
        return self.Exit[self.lang]

    def cut(self):
        return self.Cut[self.lang]

    def copy(self):
        return self.Copy[self.lang]

    def paste(self):
        return self.Paste[self.lang]

    def zoomIn(self):
        return self.ZoomIn[self.lang]

    def zoomOut(self):
        return self.ZoomOut[self.lang]

    def general(self):
        return self.General[self.lang]

    def levelPortalIn(self):
        return self.LevelPortalIn[self.lang]

    def levelPortalOut(self):
        return self.LevelPortalOut[self.lang]

    def boardItems(self):
        return self.BoardItems[self.lang]

    def movingPlatforms(self):
        return self.MovingPlatforms[self.lang]

    def enemies(self):
        return self.Enemies[self.lang]

    def lasers(self):
        return self.Lasers[self.lang]

    def states(self):
        return self.States[self.lang]

    def forceFields(self):
        return self.ForceFields[self.lang]

    def disappearingPlatforms(self):
        return self.DisappearingPlatforms[self.lang]

    def disappearingLadders(self):
        return self.DisappearingLadders[self.lang]

    def buttons(self):
        return self.Buttons[self.lang]

    def hourglasses(self):
        return self.Hourglasses[self.lang]

    def portals(self):
        return self.Portals[self.lang]

    def about(self):
        return self.About[self.lang]

    def file(self):
        return self.File[self.lang]

    def edit(self):
        return self.Edit[self.lang]

    def view(self):
        return self.View[self.lang]

    def language(self):
        return self.Language[self.lang]

    def help(self):
        return self.Help[self.lang]

    def color(self):
        return self.Color[self.lang]

    def columns(self):
        return self.Columns[self.lang]

    def timeSec(self):
        return self.TimeSec[self.lang]

    def actionForAllHourglasses(self):
        return self.ActionForAllHourglasses[self.lang]

    def stateForAllHourglasses(self):
        return self.StateForAllHourglasses[self.lang]

    def boardColorEnum(self, value: BoardColor):
        return self.BoardColorEnum[self.lang][value]

    def column(self):
        return self.Column[self.lang]

    def row(self):
        return self.Row[self.lang]

    def width(self):
        return self.Width[self.lang]

    def height(self):
        return self.Height[self.lang]

    def addNew(self):
        return self.AddNew[self.lang]

    def moveToTop(self):
        return self.MoveToTop[self.lang]

    def moveUp(self):
        return self.MoveUp[self.lang]

    def moveDown(self):
        return self.MoveDown[self.lang]

    def moveToBottom(self):
        return self.MoveToBottom[self.lang]

    def delete(self):
        return self.Delete[self.lang]

    def kind(self):
        return self.Kind[self.lang]

    def boardItemKindEnum(self, value: BoardItemKind):
        return self.BoardItemKindEnum[self.lang][value]

    def timeResist(self):
        return self.TimeResist[self.lang]

    def horizontalPosition(self):
        return self.HorizontalPosition[self.lang]

    def verticalPosition(self):
        return self.VerticalPosition[self.lang]

    def range(self):
        return self.Range[self.lang]

    def offset(self):
        return self.Offset[self.lang]

    def enemyKindEnum(self, value: EnemyKind):
        return self.EnemyKindEnum[self.lang][value]

    def timePattern(self):
        return self.TimePattern[self.lang]

    def laserKindEnum(self, value: LaserKind):
        return self.LaserKindEnum[self.lang][value]

    def turnedOn(self):
        return self.TurnedOn[self.lang]

    def buttonKindEnum(self, value: ButtonKind):
        return self.ButtonKindEnum[self.lang][value]

    def state(self):
        return self.State[self.lang]

    def duration(self):
        return self.Duration[self.lang]

    def activationMethod(self):
        return self.ActivationMethod[self.lang]

    def invertedActivation(self):
        return self.InvertedActivation[self.lang]

    def activationMethodKindEnum(self, value: ActivationMethodKind):
        return self.ActivationMethodKindEnum[self.lang][value]

    def entry(self):
        return self.Entry[self.lang]

    def portalEntryKindEnum(self, value: PortalEntryKind):
        return self.PortalEntryKindEnum[self.lang][value]

    def bool(self, value: bool):
        return self.Bool[self.lang][value]

    def problemList(self):
        return self.ProblemList[self.lang]

    def problemKindEnum(self, value: ProblemKind):
        return self.ProblemKindEnum[self.lang][value]

    def movingPlatform(self):
        return self.MovingPlatform[self.lang]

    def enemy(self):
        return self.Enemy[self.lang]

    def laser(self):
        return self.Laser[self.lang]

    def forceField(self):
        return self.ForceField[self.lang]

    def disappearingPlatform(self):
        return self.DisappearingPlatform[self.lang]

    def disappearingLadder(self):
        return self.DisappearingLadder[self.lang]

    def button(self):
        return self.Button[self.lang]

    def hourglass(self):
        return self.Hourglass[self.lang]

    def portal(self):
        return self.Portal[self.lang]

    def commandKindEnum(self, value: CommandKind):
        return self.CommandKindEnum[self.lang][value]

    def error(self):
        return self.Error[self.lang]

    def importError(self):
        return self.ImportError[self.lang]

    def exportError(self):
        return self.ExportError[self.lang]

    def loadError(self):
        return self.LoadError[self.lang]

    def saveError(self):
        return self.SaveError[self.lang]

    def testError(self):
        return self.TestError[self.lang]

    def close(self):
        return self.Close[self.lang]

    def test(self):
        return self.Test[self.lang]

    def runTest(self):
        return self.RunTest[self.lang]

    def addArea(self):
        return self.AddArea[self.lang]

    def addWall(self):
        return self.AddWall[self.lang]

    def addPlatform(self):
        return self.AddPlatform[self.lang]

    def addLadder(self):
        return self.AddLadder[self.lang]

    def addLadderWithPlatform(self):
        return self.AddLadderWithPlatform[self.lang]

    def addSpikes(self):
        return self.AddSpikes[self.lang]

    def addDecoration(self):
        return self.AddDecoration[self.lang]

    def addMovingPlatform(self):
        return self.AddMovingPlatform[self.lang]

    def addEnemy(self):
        return self.AddEnemy[self.lang]

    def addLaser(self):
        return self.AddLaser[self.lang]

    def addButton(self):
        return self.AddButton[self.lang]

    def addForceField(self):
        return self.AddForceField[self.lang]

    def addDisappearingPlatform(self):
        return self.AddDisappearingPlatform[self.lang]

    def addDisappearingLadder(self):
        return self.AddDisappearingLadder[self.lang]

    def addPortal(self):
        return self.AddPortal[self.lang]

    def addHourglass(self):
        return self.AddHourglass[self.lang]
