from data.enums import RangeKind


class PositionCalculator:

    POSITIONS_3 = [0, 0, 1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 11, 12, 12]
    POSITIONS_5 = [0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 16, 17, 17, 18, 18, 19,
                   19, 20, 20, 20]

    @staticmethod
    def calculate(position, offset, rangeKind: RangeKind):
        position = (position + offset) & rangeKind.getMask()
        if position >= rangeKind.getRange():
            position = (position - 1) ^ rangeKind.getMask()
        if rangeKind == RangeKind.Range3:
            position = PositionCalculator.POSITIONS_3[position]
        elif rangeKind == RangeKind.Range5:
            position = PositionCalculator.POSITIONS_5[position]
        return position
