from data.enums import BoardColor, BoardItemKind, RangeKind, EnemyKind, LaserKind, ActivationMethodKind, ButtonKind, \
    PortalEntryKind, ActivableItemKind


class AsmImportExportHelper:

    generalHeader = '; columns, board color, seconds'
    levelPortalsHeader = '; timePortals: colIn, rowIn, heightIn, colOut, rowOut, heightOut'
    boardItemsHeader = '; board items: kind, col, row, optional width, optional height'
    movingPlatformsHeader = '; moving platforms: rangeBegin, row, width, range, moveOffset, timeResist'
    enemiesHeader = '; enemies: rangeBegin, secondCoordinate, range, moveOffset, kind'
    lasersHeader = '; lasers: leftCol, rightCol, row, mask, kind'
    statesHeader = '; states: value, timeResist'
    activableItemsHeader = '; activable items: col, row, size, active, activationMaskOrState, kind'
    buttonsHeader = '; buttons: col, row, state, kind'
    hourglassesHeader = '; hourglasses: col, row, timeResist'
    portalsHeader = '; portals: col, row, height, kind'
    end = 'bidkEnd'

    boardColor = {
        'bcGray': BoardColor.Gray,
        'bcBrown': BoardColor.Brown,
        'bcGreen': BoardColor.Green,
        'bcBlue': BoardColor.Blue,
        'bcViolet': BoardColor.Violet,
        'bcDarkBlue': BoardColor.DarkBlue,
        'bcRed': BoardColor.Red
    }

    boardItemKind = {
        'bidkVoid': BoardItemKind.Void,
        'bidkAir': BoardItemKind.Air,
        'bidkTimeBlockade': BoardItemKind.TimeBlockade,
        'bidkWall1x1': BoardItemKind.Wall1x1,
        'bidkBottomWall1x2': BoardItemKind.BottomWall1x2,
        'bidkTopWall1x2': BoardItemKind.TopWall1x2,
        'bidkHorizontalWall': BoardItemKind.HorizontalWall,
        'bidkLongHorizontalWall': BoardItemKind.LongHorizontalWall,
        'bidkVerticalWall': BoardItemKind.VerticalWall,
        'bidkLightPlatform': BoardItemKind.LightPlatform,
        'bidkPlatform': BoardItemKind.Platform,
        'bidkLadder': BoardItemKind.Ladder,
        'bidkLadderWithPlatform': BoardItemKind.LadderWithPlatform,
        'bidkBottomSpikes': BoardItemKind.BottomSpikes,
        'bidkTopSpikes': BoardItemKind.TopSpikes,
        'bidkLeftSpikes': BoardItemKind.LeftSpikes,
        'bidkRightSpikes': BoardItemKind.RightSpikes,
        'bidkStructure': BoardItemKind.Structure,
        'bidkBottomWall': BoardItemKind.BottomWall,
        'bidkLeftWall': BoardItemKind.LeftWall,
        'bidkRightWall': BoardItemKind.RightWall,
        'bidkTopWall': BoardItemKind.TopWall,
        'bidkLeftTopWall': BoardItemKind.LeftTopWall,
        'bidkRightTopWall': BoardItemKind.RightTopWall,
        'bidkNarrowStructure': BoardItemKind.NarrowStructure,
        'bidkTopWallWithCorners': BoardItemKind.TopWallWithCorners,
        'bidkChain': BoardItemKind.Chain,
        'bidkTile': BoardItemKind.Tile,
        'bidkPattern4x4_1': BoardItemKind.Pattern4x4_1,
        'bidkPattern4x4_2': BoardItemKind.Pattern4x4_2,
        'bidkPattern4x4_3': BoardItemKind.Pattern4x4_3,
        'bidkPattern3x3_1': BoardItemKind.Pattern3x3_1,
        'bidkPattern3x3_2': BoardItemKind.Pattern3x3_2,
        'bidkPattern3x3_3': BoardItemKind.Pattern3x3_3
    }

    rangeKind = {
        'range1': RangeKind.Range1,
        'range2': RangeKind.Range2,
        'range4': RangeKind.Range4,
        'range8': RangeKind.Range8,
        'range16': RangeKind.Range16,
        'range32': RangeKind.Range32,
        'range3': RangeKind.Range3,
        'range5': RangeKind.Range5
    }

    enemyKind = {
        'ekVertical': (EnemyKind.Vertical, False),
        'ekHorizontal': (EnemyKind.Horizontal, False),
        'ekVerticalTimeResist': (EnemyKind.Vertical, True),
        'ekHorizontalTimeResist': (EnemyKind.Horizontal, True),
        'ekFollowingVerticalTimeResist': (EnemyKind.FollowingVertical, True),
        'ekFollowingHorizontalTimeResist': (EnemyKind.FollowingHorizontal, True)
    }

    laserKind = {
        'lkLeftToRight': (LaserKind.LeftToRight, False),
        'lkRightToLeft': (LaserKind.RightToLeft, False),
        'lkLeftToRightTimeResist': (LaserKind.LeftToRight, True),
        'lkRightToLeftTimeResist': (LaserKind.RightToLeft, True)
    }

    activableItemKind = {
        'aikTimePlatform': (ActivationMethodKind.Time, False, False, ActivableItemKind.DisappearingPlatform),
        'aikTimeForceField': (ActivationMethodKind.Time, False, False, ActivableItemKind.ForceField),
        'aikStatePlatform': (ActivationMethodKind.State, False, False, ActivableItemKind.DisappearingPlatform),
        'aikStateForceField': (ActivationMethodKind.State, False, False, ActivableItemKind.ForceField),
        'aikTimeInversePlatform': (ActivationMethodKind.Time, True, False, ActivableItemKind.DisappearingPlatform),
        'aikTimeInverseForceField': (ActivationMethodKind.Time, True, False, ActivableItemKind.ForceField),
        'aikStateInversePlatform': (ActivationMethodKind.State, True, False, ActivableItemKind.DisappearingPlatform),
        'aikStateInverseForceField': (ActivationMethodKind.State, True, False, ActivableItemKind.ForceField),
        'aikTimeTimeResistPlatform': (ActivationMethodKind.Time, False, True, ActivableItemKind.DisappearingPlatform),
        'aikTimeTimeResistForceField': (ActivationMethodKind.Time, False, True, ActivableItemKind.ForceField),
        'aikStateTimeResistPlatform': (ActivationMethodKind.State, False, True, ActivableItemKind.DisappearingPlatform),
        'aikStateTimeResistForceField': (ActivationMethodKind.State, False, True, ActivableItemKind.ForceField),
        'aikTimeInverseTimeResistPlatform': (ActivationMethodKind.Time, True, True, ActivableItemKind.DisappearingPlatform),
        'aikTimeInverseTimeResistForceField': (ActivationMethodKind.Time, True, True, ActivableItemKind.ForceField),
        'aikStateInverseTimeResistPlatform': (ActivationMethodKind.State, True, True, ActivableItemKind.DisappearingPlatform),
        'aikStateInverseTimeResistForceField': (ActivationMethodKind.State, True, True, ActivableItemKind.ForceField),
        'aikTimeLadder': (ActivationMethodKind.Time, False, False, ActivableItemKind.DisappearingLadder),
        'aikStateLadder': (ActivationMethodKind.State, False, False, ActivableItemKind.DisappearingLadder),
        'aikTimeInverseLadder': (ActivationMethodKind.Time, True, False, ActivableItemKind.DisappearingLadder),
        'aikStateInverseLadder': (ActivationMethodKind.State, True, False, ActivableItemKind.DisappearingLadder),
        'aikTimeTimeResistLadder': (ActivationMethodKind.Time, False, True, ActivableItemKind.DisappearingLadder),
        'aikStateTimeResistLadder': (ActivationMethodKind.State, False, True, ActivableItemKind.DisappearingLadder),
        'aikTimeInverseTimeResistLadder': (ActivationMethodKind.Time, True, True, ActivableItemKind.DisappearingLadder),
        'aikStateInverseTimeResistLadder': (ActivationMethodKind.State, True, True, ActivableItemKind.DisappearingLadder)
    }

    buttonKind = {
        'bkStateOn': (ButtonKind.TurnsOn, False),
        'bkStateOff': (ButtonKind.TurnsOff, False),
        'bkStateOnTimeResist': (ButtonKind.TurnsOn, True),
        'bkStateOffTimeResist': (ButtonKind.TurnsOff, True),
        'bkStateOnOff': (ButtonKind.TurnsOnOff, False),
        'bkStateOnOffTimeResist': (ButtonKind.TurnsOnOff, True),
        'bkStateOnTimer': (ButtonKind.TurnsOnTimer, False),
        'bkStateOffTimer': (ButtonKind.TurnsOffTimer, False),
        'bkStateOnTimerTimeResist': (ButtonKind.TurnsOnTimer, True),
        'bkStateOffTimerTimeResist': (ButtonKind.TurnsOffTimer, True),
    }

    portalKind = {
        'pkLeftEntry': PortalEntryKind.LeftSide,
        'pkRightEntry': PortalEntryKind.RightSide,
        'pkBothEntries': PortalEntryKind.BothSides,
        'pkTopEntry': PortalEntryKind.TopSide,
        'pkBottomEntry': PortalEntryKind.BottomSide
    }

    def parseBoardColor(self, text: str):
        return self.boardColor[text]

    def encodeBoardColor(self, color: BoardColor):
        return [x for x in self.boardColor if self.boardColor[x] == color][0]

    def parseBoardItemKind(self, text: str):
        return self.boardItemKind[text]

    def encodeBoardItemKind(self, kind: BoardItemKind):
        return [x for x in self.boardItemKind if self.boardItemKind[x] == kind][0]

    def parseRangeKind(self, text: str):
        return self.rangeKind[text]

    def encodeRangeKind(self, kind: RangeKind):
        return [x for x in self.rangeKind if self.rangeKind[x] == kind][0]

    def parseEnemyKind(self, text: str):
        return self.enemyKind[text]

    def encodeEnemyKind(self, kind: EnemyKind, timeResist: bool):
        return [x for x in self.enemyKind if self.enemyKind[x] == (kind, timeResist)][0]

    def parseLaserKind(self, text: str):
        return self.laserKind[text]

    def encodeLaserKind(self, kind: LaserKind, timeResist: bool):
        return [x for x in self.laserKind if self.laserKind[x] == (kind, timeResist)][0]

    def parseActivableItemKind(self, text: str):
        return self.activableItemKind[text]

    def encodeActivableItemKind(self, methodKind: ActivationMethodKind, inverted: bool, timeResist: bool,
                                itemKind: ActivableItemKind):
        return [x for x in self.activableItemKind if self.activableItemKind[x] ==
                (methodKind, inverted, timeResist, itemKind)][0]

    def parseButtonKind(self, text: str):
        return self.buttonKind[text]

    def encodeButtonKind(self, kind: ButtonKind, timeResist: bool):
        return [x for x in self.buttonKind if self.buttonKind[x] == (kind, timeResist)][0]

    def parsePortalKind(self, text: str):
        return self.portalKind[text]

    def encodePortalKind(self, kind: PortalEntryKind):
        return [x for x in self.portalKind if self.portalKind[x] == kind][0]

    @staticmethod
    def parseBool(text: str):
        match text:
            case 'False':
                return False
            case 'True':
                return True
        raise Exception('parseBool error')

    @staticmethod
    def parseBinary(text: str):
        if len(text) != 9 or text[0] != '%':
            raise Exception('parseBinary error')
        return int(text[1:], 2)

    @staticmethod
    def encodeBinary(val: int):
        return f'%{val:08b}'
