from enum import Enum


class BoardColor(str, Enum):
    Gray = 'Gray'
    Brown = 'Brown'
    Green = 'Green'
    Blue = 'Blue'
    Violet = 'Violet'
    DarkBlue = 'DarkBlue'
    Red = 'Red'


class BoardItemKind(str, Enum):
    Void = 'Void'
    Air = 'Air'
    TimeBlockade = 'TimeBlockade'
    Wall1x1 = 'Wall1x1'
    BottomWall1x2 = 'BottomWall1x2'
    TopWall1x2 = 'TopWall1x2'
    HorizontalWall = 'HorizontalWall'
    LongHorizontalWall = 'LongHorizontalWall'
    VerticalWall = 'VerticalWall'
    LightPlatform = 'LightPlatform'
    Platform = 'Platform'
    Ladder = 'Ladder'
    LadderWithPlatform = 'LadderWithPlatform'
    BottomSpikes = 'BottomSpikes'
    TopSpikes = 'TopSpikes'
    LeftSpikes = 'LeftSpikes'
    RightSpikes = 'RightSpikes'
    Structure = 'Structure'
    BottomWall = 'BottomWall'
    LeftWall = 'LeftWall'
    RightWall = 'RightWall'
    TopWall = 'TopWall'
    LeftTopWall = 'LeftTopWall'
    RightTopWall = 'RightTopWall'
    NarrowStructure = 'NarrowStructure'
    TopWallWithCorners = 'TopWallWithCorners'
    Chain = 'Chain'
    Tile = 'Tile'
    Pattern4x4_1 = 'Pattern4x4_1'
    Pattern4x4_2 = 'Pattern4x4_2'
    Pattern4x4_3 = 'Pattern4x4_3'
    Pattern3x3_1 = 'Pattern3x3_1'
    Pattern3x3_2 = 'Pattern3x3_2'
    Pattern3x3_3 = 'Pattern3x3_3'

    def containsWidth(self):
        return self in [BoardItemKind.Void, BoardItemKind.Air, BoardItemKind.TimeBlockade, BoardItemKind.Wall1x1,
                        BoardItemKind.HorizontalWall, BoardItemKind.LongHorizontalWall, BoardItemKind.LightPlatform,
                        BoardItemKind.Platform, BoardItemKind.BottomSpikes, BoardItemKind.TopSpikes,
                        BoardItemKind.BottomWall, BoardItemKind.TopWall, BoardItemKind.TopWallWithCorners]

    def containsHeight(self):
        return self in [BoardItemKind.Void, BoardItemKind.Air, BoardItemKind.TimeBlockade, BoardItemKind.Wall1x1,
                        BoardItemKind.BottomWall1x2, BoardItemKind.TopWall1x2, BoardItemKind.VerticalWall,
                        BoardItemKind.Ladder, BoardItemKind.LadderWithPlatform, BoardItemKind.Structure,
                        BoardItemKind.LeftSpikes, BoardItemKind.RightSpikes, BoardItemKind.LeftWall,
                        BoardItemKind.RightWall, BoardItemKind.NarrowStructure, BoardItemKind.Chain]

    def minWidth(self):
        match self:
            case BoardItemKind.Void | BoardItemKind.Air | BoardItemKind.TimeBlockade | BoardItemKind.Wall1x1 | \
                 BoardItemKind.HorizontalWall | BoardItemKind.LightPlatform | BoardItemKind.BottomSpikes | \
                 BoardItemKind.TopSpikes | BoardItemKind.LeftSpikes | BoardItemKind.RightSpikes | \
                 BoardItemKind.BottomWall | BoardItemKind.LeftWall | BoardItemKind.RightWall | BoardItemKind.TopWall | \
                 BoardItemKind.LeftTopWall | BoardItemKind.RightTopWall | BoardItemKind.NarrowStructure | \
                 BoardItemKind.TopWallWithCorners | BoardItemKind.Chain:
                return 1
            case BoardItemKind.BottomWall1x2 | BoardItemKind.TopWall1x2 | BoardItemKind.VerticalWall | \
                 BoardItemKind.Platform | BoardItemKind.Ladder | BoardItemKind.LadderWithPlatform | \
                 BoardItemKind.Structure | BoardItemKind.Tile:
                return 2
            case BoardItemKind.Pattern3x3_1 | BoardItemKind.Pattern3x3_2 | BoardItemKind.Pattern3x3_3 | \
                 BoardItemKind.LongHorizontalWall:
                return 3
            case BoardItemKind.Pattern4x4_1 | BoardItemKind.Pattern4x4_2 | BoardItemKind.Pattern4x4_3:
                return 4

    def minHeight(self):
        match self:
            case BoardItemKind.Void | BoardItemKind.Air | BoardItemKind.TimeBlockade | BoardItemKind.Wall1x1 | \
                 BoardItemKind.BottomWall1x2 | BoardItemKind.TopWall1x2 | BoardItemKind.VerticalWall | \
                 BoardItemKind.LightPlatform | BoardItemKind.Platform | BoardItemKind.Ladder | \
                 BoardItemKind.BottomSpikes | BoardItemKind.TopSpikes | BoardItemKind.LeftSpikes | \
                 BoardItemKind.RightSpikes | BoardItemKind.Structure | BoardItemKind.BottomWall | \
                 BoardItemKind.LeftWall | BoardItemKind.RightWall | BoardItemKind.TopWall | \
                 BoardItemKind.LeftTopWall | BoardItemKind.RightTopWall | BoardItemKind.NarrowStructure | \
                 BoardItemKind.TopWallWithCorners | BoardItemKind.Chain | BoardItemKind.Tile:
                return 1
            case BoardItemKind.HorizontalWall | BoardItemKind.LongHorizontalWall | BoardItemKind.LadderWithPlatform:
                return 2
            case BoardItemKind.Pattern3x3_1 | BoardItemKind.Pattern3x3_2 | BoardItemKind.Pattern3x3_3:
                return 3
            case BoardItemKind.Pattern4x4_1 | BoardItemKind.Pattern4x4_2 | BoardItemKind.Pattern4x4_3:
                return 4


class RangeKind(str, Enum):
    Range1 = '1'
    Range2 = '2'
    Range4 = '4'
    Range8 = '8'
    Range16 = '16'
    Range32 = '32'
    Range3 = '3'
    Range5 = '5'

    def getValue(self):
        data = {
            RangeKind.Range1: 1,
            RangeKind.Range2: 2,
            RangeKind.Range4: 4,
            RangeKind.Range8: 8,
            RangeKind.Range16: 16,
            RangeKind.Range32: 32,
            RangeKind.Range3: 3,
            RangeKind.Range5: 5
        }
        return data[self]

    def getRange(self):
        data = {
            RangeKind.Range1: 4,
            RangeKind.Range2: 8,
            RangeKind.Range4: 16,
            RangeKind.Range8: 32,
            RangeKind.Range16: 64,
            RangeKind.Range32: 128,
            RangeKind.Range3: 16,
            RangeKind.Range5: 32
        }
        return data[self]

    def getMask(self):
        data = {
            RangeKind.Range1: 7,
            RangeKind.Range2: 15,
            RangeKind.Range4: 31,
            RangeKind.Range8: 63,
            RangeKind.Range16: 127,
            RangeKind.Range32: 255,
            RangeKind.Range3: 31,
            RangeKind.Range5: 63
        }
        return data[self]


class EnemyKind(str, Enum):
    Horizontal = 'Horizontal'
    Vertical = 'Vertical'
    FollowingHorizontal = 'FollowingHorizontal'
    FollowingVertical = 'FollowingVertical'

    def isFollowing(self):
        return self in [EnemyKind.FollowingHorizontal, EnemyKind.FollowingVertical]


class LaserKind(str, Enum):
    LeftToRight = 'LeftToRight'
    RightToLeft = 'RightToLeft'


class ActivableItemKind(str, Enum):
    ForceField = 'ForceField',
    DisappearingPlatform = 'DisappearingPlatform',
    DisappearingLadder = 'DisappearingLadder'


class ActivationMethodKind(str, Enum):
    Time = 'Time'
    State = 'State'


class ButtonKind(str, Enum):
    TurnsOn = 'TurnsOn'
    TurnsOff = 'TurnsOff'
    TurnsOnOff = 'TurnsOnOff'
    TurnsOnTimer = 'TurnsOnTimer'
    TurnsOffTimer = 'TurnsOffTimer'

    def isTimer(self):
        return self in [ButtonKind.TurnsOnTimer, ButtonKind.TurnsOffTimer]


class PortalEntryKind(str, Enum):
    LeftSide = 'LeftSide'
    RightSide = 'RightSide'
    BothSides = 'BothSides'
    TopSide = 'TopSide'
    BottomSide = 'BottomSide'

    def isVertical(self):
        return self in [PortalEntryKind.LeftSide, PortalEntryKind.RightSide, PortalEntryKind.BothSides]


class ProblemKind(str, Enum):
    OutsideBoard = 'OutsideBoard'
    LackOfHourglasses = 'LackOfHourglasses'
    PortalWithoutPair = 'PortalWithoutPair'
    PortalPairOrientation = 'PortalPairOrientation'
    ObjectOverlap = 'ObjectOverlap'
    StateDoesNotExist = 'StateDoesNotExist'
    TooManyMovingPlatforms = 'TooManyMovingPlatforms'
    TooManyEnemies = 'TooManyEnemies'
    TooManyLasers = 'TooManyLasers'
    TooManyActivableItems = 'TooManyActivableItems'
    TooManyButtons = 'TooManyButtons'
    TooManyStates = 'TooManyStates'
    TooManyHourglasses = 'ToManyHourglasses'
    TooManyPortals = 'TooManyPortals'
    InvalidTimeResist = 'InvalidTimeResist'
    FollowingEnemyNotTimeResist = 'FollowingEnemyNotTimeResist'
    TimerButtonNotTimeResist = 'TimerButtonNotTimeResist'
    StateUsedForAllHourglasses = 'StateUsedForAllHourglasses'


class ResizeObjectMode(int, Enum):
    NoResize = 0
    ResizeOnRight = 1
    ResizeOnBottom = 2
    ResizeOnRightBottom = 3
