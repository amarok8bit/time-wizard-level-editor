#!/bin/sh
# Create a folder (named dmg) to prepare our DMG in (if it doesn't already exist).
mkdir -p rel/dmg
# Empty the dmg folder.
rm -r rel/dmg/*
# Copy the app bundle to the dmg folder.
cp -R "dist/Time Wizard Level Editor.app" rel/dmg
# If the DMG already exists, delete it.
test -f "rel/Time Wizard Level Editor.dmg" && rm "rel/Time Wizard Level Editor.dmg"
create-dmg \
  --volname "Time Wizard Level Editor" \
  --volicon "release/macos/timewizard32x32.icns" \
  --window-pos 200 120 \
  --window-size 600 300 \
  --icon-size 100 \
  --icon "Time Wizard Level Editor.app" 175 120 \
  --hide-extension "Time Wizard Level Editor.app" \
  --app-drop-link 425 120 \
  "rel/Time Wizard Level Editor.dmg" \
  "rel/dmg/"

