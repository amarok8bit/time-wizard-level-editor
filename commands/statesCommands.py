from commands.baseCommands import BaseUndoChangeCommand
from commands.commandKind import CommandKind


class StateTimeResistChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.timeResist = self.newValue

    def undo(self):
        self.data.timeResist = self.oldValue

    def getCommandKind(self):
        return CommandKind.StateTimeResist


class StateOnChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.on = self.newValue

    def undo(self):
        self.data.on = self.oldValue

    def getCommandKind(self):
        return CommandKind.StateOn
