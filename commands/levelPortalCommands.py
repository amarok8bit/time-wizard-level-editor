from commands.baseCommands import BaseUndoChangeCommand
from commands.commandKind import CommandKind


class LevelPortalPosChangeCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.column = self.newValue.x()
        self.data.row = self.newValue.y()

    def undo(self):
        self.data.column = self.oldValue.x()
        self.data.row = self.oldValue.y()


class LevelPortalInPosChangeCommand(LevelPortalPosChangeCommand):

    def getCommandKind(self):
        return CommandKind.LevelPortalInPosition


class LevelPortalOutPosChangeCommand(LevelPortalPosChangeCommand):

    def getCommandKind(self):
        return CommandKind.LevelPortalOutPosition


class LevelPortalSizeChangeCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.height = self.newValue.height()

    def undo(self):
        self.data.height = self.oldValue.height()


class LevelPortalInSizeChangeCommand(LevelPortalSizeChangeCommand):

    def getCommandKind(self):
        return CommandKind.LevelPortalInSize


class LevelPortalOutSizeChangeCommand(LevelPortalSizeChangeCommand):

    def getCommandKind(self):
        return CommandKind.LevelPortalOutSize
