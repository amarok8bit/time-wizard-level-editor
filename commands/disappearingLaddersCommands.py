from commands.baseCommands import BaseUndoChangeCommand
from commands.commandKind import CommandKind


class DisappearingLadderActivationMethodChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.methodKind = self.newValue

    def undo(self):
        self.data.methodKind = self.oldValue

    def getCommandKind(self):
        return CommandKind.DisappearingLadderActivationMethod


class DisappearingLadderInvertedChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.invertedActivation = self.newValue

    def undo(self):
        self.data.inverted = self.oldValue

    def getCommandKind(self):
        return CommandKind.DisappearingLadderInverted


class DisappearingLadderTimeResistChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.timeResist = self.newValue

    def undo(self):
        self.data.timeResist = self.oldValue

    def getCommandKind(self):
        return CommandKind.DisappearingLadderTimeResist


class DisappearingLadderPosChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.column = self.newValue.x()
        self.data.row = self.newValue.y()

    def undo(self):
        self.data.column = self.oldValue.x()
        self.data.row = self.oldValue.y()

    def getCommandKind(self):
        return CommandKind.DisappearingLadderPosition


class DisappearingLadderSizeChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.height = self.newValue.height()

    def undo(self):
        self.data.height = self.oldValue.height()

    def getCommandKind(self):
        return CommandKind.DisappearingLadderSize


class DisappearingLadderMaskChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.mask = self.newValue

    def undo(self):
        self.data.mask = self.oldValue

    def getCommandKind(self):
        return CommandKind.DisappearingLadderMask


class DisappearingLadderStateChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.state = self.newValue

    def undo(self):
        self.data.state = self.oldValue

    def getCommandKind(self):
        return CommandKind.DisappearingLadderState
