from commands.baseCommands import BaseUndoChangeCommand
from commands.commandKind import CommandKind


class DisappearingPlatformActivationMethodChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.methodKind = self.newValue

    def undo(self):
        self.data.methodKind = self.oldValue

    def getCommandKind(self):
        return CommandKind.DisappearingPlatformActivationMethod


class DisappearingPlatformInvertedChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.invertedActivation = self.newValue

    def undo(self):
        self.data.inverted = self.oldValue

    def getCommandKind(self):
        return CommandKind.DisappearingPlatformInverted


class DisappearingPlatformTimeResistChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.timeResist = self.newValue

    def undo(self):
        self.data.timeResist = self.oldValue

    def getCommandKind(self):
        return CommandKind.DisappearingPlatformTimeResist


class DisappearingPlatformPosChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.column = self.newValue.x()
        self.data.row = self.newValue.y()

    def undo(self):
        self.data.column = self.oldValue.x()
        self.data.row = self.oldValue.y()

    def getCommandKind(self):
        return CommandKind.DisappearingPlatformPosition


class DisappearingPlatformSizeChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.width = self.newValue.width()

    def undo(self):
        self.data.width = self.oldValue.width()

    def getCommandKind(self):
        return CommandKind.DisappearingPlatformSize


class DisappearingPlatformMaskChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.mask = self.newValue

    def undo(self):
        self.data.mask = self.oldValue

    def getCommandKind(self):
        return CommandKind.DisappearingPlatformMask


class DisappearingPlatformStateChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.state = self.newValue

    def undo(self):
        self.data.state = self.oldValue

    def getCommandKind(self):
        return CommandKind.DisappearingPlatformState
