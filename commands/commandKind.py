from enum import Enum


class CommandKind(int, Enum):

    GeneralBoardColor = 100
    GeneralBoardColumns = 101
    GeneralLevelTime = 102
    ActionAfterAllHourglasses = 103
    StateAfterAllHourglasses = 104

    LevelPortalInPosition = 200
    LevelPortalInSize = 201

    LevelPortalOutPosition = 300
    LevelPortalOutSize = 301

    BoardItemKind = 400
    BoardItemPosition = 401
    BoardItemSize = 402

    MovingPlatformTimeResist = 500
    MovingPlatformPosition = 501
    MovingPlatformSize = 502
    MovingPlatformRange = 503
    MovingPlatformMoveOffset = 504

    EnemyKind = 600
    EnemyTimeResist = 601
    EnemyPosition = 602
    EnemyRange = 603
    EnemyRangeValue = 604
    EnemyMoveOffset = 605

    LaserKind = 700
    LaserTimeResist = 701
    LaserPosition = 702
    LaserSize = 703
    LaserMask = 704

    StateTimeResist = 800
    StateOn = 801

    ButtonKind = 900
    ButtonTimeResist = 901
    ButtonPosition = 902
    ButtonState = 903
    ButtonDuration = 904

    ForceFieldActivationMethod = 1000
    ForceFieldInverted = 1001
    ForceFieldTimeResist = 1002
    ForceFieldPosition = 1003
    ForceFieldSize = 1004
    ForceFieldMask = 1005
    ForceFieldState = 1006

    DisappearingPlatformActivationMethod = 1100
    DisappearingPlatformInverted = 1101
    DisappearingPlatformTimeResist = 1102
    DisappearingPlatformPosition = 1103
    DisappearingPlatformSize = 1104
    DisappearingPlatformMask = 1105
    DisappearingPlatformState = 1106

    DisappearingLadderActivationMethod = 1200
    DisappearingLadderInverted = 1201
    DisappearingLadderTimeResist = 1202
    DisappearingLadderPosition = 1203
    DisappearingLadderSize = 1204
    DisappearingLadderMask = 1205
    DisappearingLadderState = 1206

    HourglassTimeResist = 1300
    HourglassPosition = 1301

    PortalKind = 1400
    PortalPosition = 1401
    PortalSize = 1402

    ListAddItem = 1500
    ListRemoveItem = 1501
    ListMoveItem = 1502

    GroupTimeResist = 1500
