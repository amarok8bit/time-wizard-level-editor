from abc import abstractmethod

from PyQt6.QtCore import Qt, QSize
from PyQt6.QtGui import QAction, QUndoStack
from PyQt6.QtWidgets import QVBoxLayout, QToolBar, QListWidget, QWidget, QGridLayout, QListWidgetItem, QSpinBox, \
    QStyledItemDelegate

from commands.itemListCommands import AddItemCommand, RemoveItemCommand, MoveItemCommand
from data.language import Language
from data.levelInfo import ItemInfo
from data.selectionManager import SelectionManager
from gui.headerWidget import HeaderWidget, QHLine
from gui.iconProvider import IconProvider
from gui.toolPanelWidget import ToolPanelWidget


class BinarySpinBox(QSpinBox):
    def textFromValue(self, value):
        return "{:08b}".format(value)


class ItemNameDelegate(QStyledItemDelegate):

    def __init__(self, parent, lang: Language):
        super().__init__(parent)
        self.lang = lang

    def initStyleOption(self, option, index):
        super().initStyleOption(option, index)
        option.text =\
            f'{index.row()}. {option.widget.item(index.row()).data(Qt.ItemDataRole.UserRole).langStr(self.lang)}'


class ListWithControls(QWidget):

    def __init__(self, iconProvider: IconProvider, lang: Language):
        self.lang = lang
        super().__init__()

        layout = QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setAlignment(Qt.AlignmentFlag.AlignTop)

        toolBar = QToolBar('Control', self)
        toolBar.setMovable(False)

        self.addAction = QAction(iconProvider.get(':icons/items-add'), None)
        self.topAction = QAction(iconProvider.get(':icons/items-top'), None)
        self.upAction = QAction(iconProvider.get(':icons/items-up'), None)
        self.downAction = QAction(iconProvider.get(':icons/items-down'), None)
        self.bottomAction = QAction(iconProvider.get(':icons/items-bottom'), None)
        self.deleteAction = QAction(iconProvider.get(':icons/items-delete'), None)
        toolBar.addAction(self.addAction)
        toolBar.addAction(self.topAction)
        toolBar.addAction(self.upAction)
        toolBar.addAction(self.downAction)
        toolBar.addAction(self.bottomAction)
        toolBar.addAction(self.deleteAction)
        toolBar.setIconSize(QSize(24, 24))

        layout.addWidget(toolBar)
        self.itemsList = QListWidget()
        layout.addWidget(self.itemsList)

        self.setLayout(layout)

        delegate = ItemNameDelegate(self.itemsList, self.lang)
        self.itemsList.setItemDelegate(delegate)

    def updateLanguage(self):
        self.addAction.setText(self.lang.addNew())
        self.topAction.setText(self.lang.moveToTop())
        self.upAction.setText(self.lang.moveUp())
        self.downAction.setText(self.lang.moveDown())
        self.bottomAction.setText(self.lang.moveToBottom())
        self.deleteAction.setText(self.lang.delete())

    def setItems(self, items):
        self.itemsList.addItems(items)


class ItemListToolPanel(ToolPanelWidget):

    @abstractmethod
    def headerLabel(self):
        pass

    @abstractmethod
    def headerIcon(self):
        pass

    @abstractmethod
    def initControls(self, layout: QGridLayout):
        pass

    def __init__(self, data: list, history: QUndoStack, selectionManager: SelectionManager, iconProvider: IconProvider,
                 lang: Language):
        super().__init__(history, selectionManager, lang)
        self.data = data
        self.lang = lang

        self.layout = QGridLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setAlignment(Qt.AlignmentFlag.AlignTop)
        self.header = HeaderWidget(iconProvider, None, self.headerIcon())
        self.layout.addWidget(self.header, 0, 0, 1, 2)
        self.layout.addWidget(QHLine(), 1, 0, 1, 2)

        self.itemsList = ListWithControls(iconProvider, self.lang)
        self.layout.addWidget(self.itemsList, 2, 0, 1, 2)

        self.initControls(self.layout)
        self.setLayout(self.layout)

        self.itemsList.itemsList.itemClicked.connect(self.selectionChanged)
        self.itemsList.itemsList.currentRowChanged.connect(self.selectionChanged)
        self.fillItems()

        self.selectionChanged()
        self.itemsList.addAction.triggered.connect(self.addItem)
        self.itemsList.topAction.triggered.connect(self.moveToTop)
        self.itemsList.upAction.triggered.connect(self.moveUp)
        self.itemsList.downAction.triggered.connect(self.moveDown)
        self.itemsList.bottomAction.triggered.connect(self.moveToBottom)
        self.itemsList.deleteAction.triggered.connect(self.deleteItem)

    def updateLanguage(self):
        self.header.setText(self.headerLabel())
        self.itemsList.updateLanguage()
        self.internalUpdateLanguage()

    @abstractmethod
    def internalUpdateLanguage(self):
        pass

    def showTab(self):
        self.selectionManager.setSelected(self.getCurrItem())

    def clearSelection(self):
        self.itemsList.itemsList.setCurrentRow(-1)

    def setSelection(self, item: ItemInfo):
        if not (item in self.data):
            self.clearSelection()
        else:
            row = self.data.index(item)
            if row < 0 or row >= len(self.data):
                self.clearSelection()
            else:
                self.itemsList.itemsList.setCurrentRow(row)

    def fillItems(self):
        self.itemsList.itemsList.clear()

        for x in self.data:
            item = QListWidgetItem()
            item.setText(str(x))
            item.setData(Qt.ItemDataRole.UserRole, x)
            self.itemsList.itemsList.addItem(item)
        if len(self.data) > 0:
            self.itemsList.itemsList.setCurrentRow(0)

    def getCurrItem(self):
        if self.updating:
            return None

        row = self.getCurrentRow()
        if row is None:
            return None

        if row < 0 or row >= len(self.data):
            return None

        return self.data[row]

    def getCurrentItem(self):
        selected = self.itemsList.itemsList.selectedItems()
        if len(selected) > 0:
            return selected[0]
        else:
            return None

    def getCurrentItemData(self):
        selected = self.itemsList.itemsList.selectedItems()
        if len(selected) > 0:
            return selected[0].data(Qt.ItemDataRole.UserRole)
        else:
            return None

    def getCurrentRow(self):
        selected = self.itemsList.itemsList.selectedItems()
        if len(selected) > 0:
            return self.itemsList.itemsList.currentRow()
        else:
            return None

    def selectionChanged(self):
        if self.getCurrentItemData() is None:
            self.itemsList.topAction.setEnabled(False)
            self.itemsList.upAction.setEnabled(False)
            self.itemsList.downAction.setEnabled(False)
            self.itemsList.bottomAction.setEnabled(False)
            self.itemsList.deleteAction.setEnabled(False)
        else:
            row = self.getCurrentRow()
            count = self.itemsList.itemsList.count()
            self.itemsList.topAction.setEnabled(row > 0)
            self.itemsList.upAction.setEnabled(row > 0)
            self.itemsList.downAction.setEnabled(row < count - 1)
            self.itemsList.bottomAction.setEnabled(row < count - 1)
            self.itemsList.deleteAction.setEnabled(True)

        if self.isVisible():
            self.selectionManager.setSelected(self.getCurrItem())
        self.updateControls()

    def reinitControls(self):
        self.fillItems()
        self.updateControls()

    def updateControls(self):
        super().updateControls()
        self.itemsList.itemsList.setItemDelegate(ItemNameDelegate(self.itemsList.itemsList, self.lang))

    @abstractmethod
    def createNewItem(self):
        pass

    def addItem(self):
        item = self.createNewItem()
        lastRow = self.itemsList.itemsList.count()

        command = AddItemCommand(self.data, item, lastRow, self.itemsList.itemsList, self.lang)
        self.history.push(command)

    def moveItem(self, toRow):
        row = self.getCurrentRow()
        count = self.itemsList.itemsList.count()
        if (row is None) or (row == toRow) or (toRow < 0) or (toRow >= count):
            return

        item = self.data[row]

        command = MoveItemCommand(self.data, item, row, toRow, self.itemsList.itemsList, self.lang)
        self.history.push(command)

    def moveToTop(self):
        self.moveItem(0)

    def moveUp(self):
        self.moveItem(self.getCurrentRow() - 1)

    def moveDown(self):
        self.moveItem(self.getCurrentRow() + 1)

    def moveToBottom(self):
        self.moveItem(self.itemsList.itemsList.count() - 1)

    def deleteItem(self):
        row = self.getCurrentRow()
        if row is None:
            return

        command = RemoveItemCommand(self.data, self.data[row], row, self.itemsList.itemsList, self.lang)
        self.history.push(command)
