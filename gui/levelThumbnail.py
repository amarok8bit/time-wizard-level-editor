from PyQt6.QtWidgets import QLabel

from image.thumbnailGenerator import ThumbnailGenerator


class LevelThumbnail(QLabel):

    def __init__(self, generator: ThumbnailGenerator):
        super().__init__()
        self.generator = generator

    def updateThumbnail(self):
        self.generator.generate()
        self.setPixmap(self.generator.image)

