from PyQt6.QtCore import QSize
from PyQt6.QtWidgets import QWidget, QHBoxLayout, QLabel, QFrame

from gui.iconProvider import IconProvider


class QHLine(QFrame):
    def __init__(self):
        super(QHLine, self).__init__()
        self.setFrameShape(QFrame.Shape.HLine)
        self.setFrameShadow(QFrame.Shadow.Sunken)


class HeaderWidget(QWidget):
    IconSize = QSize(24, 24)
    HorizontalSpacing = 2

    def __init__(self, iconProvider: IconProvider, text, icon_str):
        super().__init__()

        layout = QHBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)

        self.icon = QLabel()
        self.icon.setPixmap(iconProvider.get(icon_str).pixmap(self.IconSize))

        layout.addWidget(self.icon)
        layout.addSpacing(self.HorizontalSpacing)
        self.label = QLabel(text)
        font = self.label.font()
        font.setPointSizeF(font.pointSize() * 1.5)
        self.label.setFont(font)
        layout.addWidget(self.label)
        layout.addStretch()

    def setText(self, text: str):
        self.label.setText(text)

