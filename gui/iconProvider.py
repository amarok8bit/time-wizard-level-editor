from PyQt6.QtGui import QIcon, QPalette


class IconProvider:
    def __init__(self):
        palette = QPalette()
        if palette.button().color().lightness() < 128:
            self.suffix = '-dark'
        else:
            self.suffix = ''

    def get(self, name: str):
        return QIcon(name + self.suffix)
